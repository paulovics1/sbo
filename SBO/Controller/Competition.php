<?php

namespace SBO\Controller;

use MeekroDBException;
use SBO\Model\Competition as CompetitionModel;

require_once "../scripts/config.php";

class Competition
{
    const table_name = "competition";

    public static function createAction($name, $type_id, $skater_ids)
    {
        $response = [
            "messages" => [],
            "success" => false
        ];

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (!isset($_SESSION['logged_user_id'])) {
            $response['messages']['global'] = "You have to be logged in in order to create competitions.";
        }

        $name = trim($name);

        if (!preg_match('/[A-Za-z0-9]/', $name)) {
            $response['messages']['name'] = "The name must contain at least one alphanumeric character.";
        }

        if ($type_id === CompetitionModel::TYPE_GAME_OF_SKATE) {
            if (count($skater_ids) < 2 || count($skater_ids) > 8) {
                $response['messages']['skaters'] = "Invalid number of skaters.";
            }
        } else if ($type_id === CompetitionModel::TYPE_GAME_OF_SKATE) {
            if (count($skater_ids) !== 4 && count($skater_ids) !== 8 && ($skater_ids) !== 16) {
                $response['messages']['skaters'] = "Invalid number of skaters.";
            }
        }

        if (sizeof($response['messages']) === 0) {
            try {
                CompetitionModel::insert($name, $type_id, $_SESSION['logged_user_id'], $skater_ids);
                $response['messages']['global'] = "The competition has been successfully created.";
                $response['success'] = true;
            } catch (MeekroDBException $e) {
                switch ($e->getCode()) {
                    case 1062:
                        $response['messages']['username'] = "This username is already taken.";
                        break;
                    default:
                        $response['messages']['global'] = $e->getMessage();
                }
            }
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }

    public static function fetchFilteredCompetitionIdsAction($search, $filter)
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (!isset($_SESSION['logged_user_id'])) {
            $filter = CompetitionModel::FILTER_ALL;
        }

        try {
            $response['competition_ids'] = CompetitionModel::selectFilteredIds($search, $filter, isset($_SESSION['logged_user_id']) ? $_SESSION['logged_user_id'] : null);
            $response['competition_ids'] = array_map("intval", $response['competition_ids']);
        } catch (MeekroDBException $e) {
            $response['messages']['global'] = $e->getMessage();
            $response['competition_ids'] = [];
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }

    public static function fetchSelectedCompetitionsAction($competition_ids)
    {
        try {
            $response['competitions'] = CompetitionModel::selectById($competition_ids);
        } catch (MeekroDBException $e) {
            $response['messages']['global'] = $e->getMessage();
            $response['competitions'] = [];
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }
}