<?php


namespace SBO\Controller;

use MeekroDBException;
use SBO\Model\Game as GameModel;


class Game
{
    public static function fetchCompetitionGamesAction($competition_id)
    {
        try {
            $response['games'] = GameModel::selectByCompetitionId($competition_id);
        } catch (MeekroDBException $e) {
            $response['messages']['global'] = $e->getMessage();
            $response['games'] = [];
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }
}