<?php

namespace SBO\Controller;

use MeekroDBException;
use SBO\Model\User as UserModel;
use SBO\Utils;

require_once "../scripts/config.php";

class User
{
    public static function registerAction($first_name, $last_name, $email, $username, $password)
    {
        $response = [
            "messages" => []
        ];

        $first_name = trim($first_name);
        $last_name = trim($last_name);
        $username = trim($username);
        $email = trim($email);

        $replaced_first_name = str_replace([' ', "'", '-'], '', $first_name);
        if (strlen($replaced_first_name) === 0) {
            $response['messages']['first_name'] = "The first name must contain at least one alphabetical character.";
        } elseif (!ctype_alpha($first_name[0])) {
            $response['messages']['first_name'] = "The first name must start with an alphabetical character.";
        } elseif (!ctype_alpha($replaced_first_name)) {
            $response['messages']['first_name'] = "The first name must consist of alphabetical characters, apostrophes and dashes only.";
        }


        $replaced_last_name = str_replace([' ', "'", '-'], '', $last_name);
        if (strlen($replaced_last_name) === 0) {
            $response['messages']['last_name'] = "The last name must contain at least one alphabetical character.";
        } elseif (!ctype_alpha($last_name[0])) {
            $response['messages']['last_name'] = "The last name must start with an alphabetical character.";
        } elseif (!ctype_alpha($replaced_last_name)) {
            $response['messages']['last_name'] = "The last name must consist of alphabetical characters, apostrophes and dashes only.";
        }


        $replaced_username = str_replace(['-', '_', '.'], '', $username);
        if (!ctype_alnum($replaced_username)) {
            $response['messages']['username'] = "The username must consist of alphanumerical characters, dots, underscores and dashes only.";
        } elseif (!preg_match("/[a-z]/i", $replaced_username)) {
            $response['messages']['username'] = "The username must contain at least one alphabetical character.";
        }


        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $response['messages']['email'] = "The email is not valid.";
        }


        if (strlen($password) < 6) {
            $response['messages']['password'] = "The password must consist of at least 6 characters.";
        }

        if (sizeof($response['messages']) === 0) {
            try {
                UserModel::insert($username, UserModel::TYPE_BASIC, $password, $email, $first_name, $last_name);
                $response['messages']['global'] = "Your account has been successfully registered. You can log in now.";
            } catch (MeekroDBException $e) {
                switch ($e->getCode()) {
                    case 1062:
                        $response['messages']['username'] = "This username is already taken.";
                        break;
                    default:
                        $response['messages']['global'] = $e->getMessage();
                }
            }
        }

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }

    public static function logInAction($username, $password, $remember)
    {
        $username = trim($username);
        header('Content-type:application/json;charset=utf-8');
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_COOKIE["auth_sid"]) && isset($_COOKIE["auth_token"])) {
            UserModel::delete_auth(null, $_COOKIE["auth_sid"], $_COOKIE["auth_token"]);
        }
        Utils::unsetCookie("auth_sid");
        Utils::unsetCookie("auth_token");

        $response = [
            "messages" => []
        ];

        $user = \DB::queryFirstRow("SELECT id, password FROM user WHERE username = %s", $username);

        if (is_null($user)) {
            $response['messages']['username'] = "This username is not registered in the system.";
            $response['success'] = false;
        } elseif (!password_verify($password, $user['password'])) {
            $response['messages']['password'] = "Incorrect password.";
            $response['success'] = false;
        } else {
            $_SESSION['logged_user_id'] = $user['id'];

            if ($remember) {
                $sid = bin2hex(random_bytes(12));
                $token = bin2hex(random_bytes(12));

                setcookie("auth_sid", $sid, time() + 60 * 60 * 24 * 60, "/");
                setcookie("auth_token", $token, time() + 60 * 60 * 24 * 60, "/");

                try {
                    UserModel::insert_auth($user['id'], $sid, $token);
                } catch (MeekroDBException $e) {
                    $response['messages']['global'] = $e->getMessage();
                }
            }

            $response['success'] = true;
            $response['messages']['global'] = "You have been successfully logged in.";
        }

        echo json_encode($response);
    }

    public static function logOutAction()
    {
        $response = [
            "messages" => []
        ];

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        unset($_SESSION['logged_user_id']);


        if (isset($_COOKIE["auth_sid"]) && isset($_COOKIE["auth_token"])) {
            UserModel::delete_auth(null, $_COOKIE["auth_sid"], $_COOKIE["auth_token"]);
        }

        Utils::unsetCookie("auth_sid");
        Utils::unsetCookie("auth_token");

        $response['success'] = true;
        $response['messages']['global'] = "You have been successfully logged out.";

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }


    public static function fetchLoggedUserDataAction()
    {
        $response = [
            "messages" => []
        ];

        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }

        if (isset($_COOKIE["auth_sid"]) && isset($_COOKIE["auth_token"])) {
            $authentication = UserModel::get_auth(null, $_COOKIE["auth_sid"], null)[0];

            if (is_null($authentication)) {
                Utils::unsetCookie("auth_sid");
                Utils::unsetCookie("auth_token");
            } else {
                if ($authentication['hashed_validator'] === hash("sha256", $_COOKIE["auth_token"])) {
                    if (time() - strtotime($authentication['updated']) <= 60 * 60 * 24 * 60) {
                        $token = bin2hex(random_bytes(12));

                        setcookie("auth_sid", $_COOKIE["auth_sid"], time() + 60 * 60 * 24 * 60, "/");
                        setcookie("auth_token", $token, time() + 60 * 60 * 24 * 60, "/");

                        try {
                            UserModel::update_auth_token($authentication['user_id'], $_COOKIE["auth_sid"], $token);
                        } catch (MeekroDBException $e) {
                            $response['messages']['global'] = $e->getMessage();
                        }

                        $user = UserModel::get($authentication["user_id"])[0];

                        $_SESSION['logged_user_id'] = $authentication["user_id"];
                    } else {
                        UserModel::delete_auth(null, $_COOKIE["auth_sid"]);
                    }
                } else {
                    UserModel::delete_auth($authentication['user_id']);
                    unset($_SESSION['logged_user_id']);
                }
            }
        } else if (isset($_SESSION['logged_user_id'])) {
            $user = UserModel::get($_SESSION['logged_user_id'])[0];
        }


        $response["loggedUser"] = isset($user) ? $user : null;

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }

    public static function fetchAllUsersDataAction()
    {
        $response = [
            "messages" => []
        ];
        $response["users"] = UserModel::getAllSorted();

        header('Content-type:application/json;charset=utf-8');
        echo json_encode($response);
    }
}