<?php


namespace SBO\Model;


class Competition
{
    const TYPE_GAME_OF_SKATE = 0;
    const TYPE_TOURNAMENT = 1;

    const FILTER_ALL = 0;
    const FILTER_ATTENDED_BY_ME = 1;
    const FILTER_ORGANIZED_BY_ME = 2;

    const table_name = "competition";

    public static function insert($name, $type_id, $referee_id, $skater_ids)
    {
        \DB::startTransaction();
        \DB::insert(Competition::table_name, [
            'name' => $name,
            'type_id' => $type_id,
            'referee_id' => $referee_id,
            'created_time' => \DB::sqlEval("CURRENT_TIMESTAMP()")
        ]);

        $competition_id = \DB::insertId();

        if ($type_id === Competition::TYPE_GAME_OF_SKATE) {
            Game::insert($competition_id, $skater_ids);
        } else if ($type_id === Competition::TYPE_TOURNAMENT) {
            for ($game_index = 0; $game_index < count($skater_ids) / 2; $game_index++) {
                Game::insert($competition_id, [$skater_ids[$game_index * 2], $skater_ids[$game_index * 2 + 1]], 0, $game_index);
            }
        }
        \DB::commit();
    }

    public static function selectById($competition_ids)
    {
        $result = \DB::query("SELECT * FROM " . Competition::table_name . " WHERE id IN %li", $competition_ids);

        foreach ($result as &$competition) {
            $competition["id"] = intval($competition["id"]);
            $competition["referee_id"] = intval($competition["referee_id"]);
            $competition["type_id"] = intval($competition["type_id"]);
        }

        return $result;
    }

    public static function selectFilteredIds($search, $filter, $user_id)
    {
        if ($filter === Competition::FILTER_ATTENDED_BY_ME) {
            $result = \DB::queryFirstColumn("SELECT DISTINCT c.id FROM " . Competition::table_name . " as c JOIN (" . Game::table_name . " as g, " . Game::game_skaters_table_name . " as r) ON (c.`name` LIKE %ss AND g.competition_id = c.id AND r.game_id = g.id AND r.skater_id = %i)", $search, $user_id);
        } else {
            $whereClause = new \WhereClause("and");
            $whereClause->add("`name` LIKE %ss", $search);

            if ($filter === Competition::FILTER_ORGANIZED_BY_ME) {

                $whereClause->add("`referee_id` = %i", $user_id);
            }
            $result = \DB::queryFirstColumn("SELECT id FROM " . Competition::table_name . " WHERE `name` LIKE %ss", $search);
        }

        $result = array_map("intval", $result);
        return $result;
    }
}