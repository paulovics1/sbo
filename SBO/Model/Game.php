<?php


namespace SBO\Model;


class Game
{
    const table_name = "game";
    const game_skaters_table_name = "r_game_skaters";

    public static function insert($competition_id, $skater_ids, $stage = null, $game_index = null)
    {
        \DB::insert(Game::table_name, [
            'competition_id' => $competition_id,
            'stage' => $stage,
            'game_index' => $game_index
        ]);

        $game_id = \DB::insertId();

        $index = 0;
        foreach ($skater_ids as $skater_id) {
            \DB::insert(Game::game_skaters_table_name, [
                'skater_id' => $skater_id,
                'game_id' => $game_id,
                'skater_order' => $index
            ]);
            $index++;
        }
    }

    public static function selectByCompetitionId($competition_id)
    {
        $result = \DB::query("SELECT g.*, r.ord as skaters_order FROM " . Game::table_name . " as g LEFT JOIN (SELECT r.game_id, GROUP_CONCAT(r.skater_id ORDER BY r.skater_order ASC) as ord FROM " . Game::game_skaters_table_name . " as r WHERE r.skater_order IS NOT NULL GROUP BY r.game_id) as r ON (r.game_id = g.id) WHERE competition_id = %i", $competition_id);


        foreach ($result as &$game) {
            $game["id"] = intval($game["id"]);
            $game["competition_id"] = intval($game["competition_id"]);

            if (!is_null($game["stage"])) {
                $game["stage"] = intval($game["stage"]);
            }

            if (!is_null($game["stage"])) {
                $game["game_index"] = intval($game["game_index"]);
            }

            if (is_null($game["skaters_order"])) {
                $game["skaters_order"] = [];
            } else {
                $game["skaters_order"] = array_map("intval", explode(",", $game["skaters_order"]));
            }
        }

        return $result;
    }
}