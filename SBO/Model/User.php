<?php


namespace SBO\Model;


class User
{
    const TYPE_SUPER_ADMIN = 0;
    const TYPE_ADMIN = 1;
    const TYPE_BASIC = 2;

    const table_name = "user";
    const auth_tokens_table_name = "auth_tokens";

    public static function insert($username, $type, $password, $email, $first_name, $last_name)
    {
        return \DB::insert(User::table_name, [
            'username' => $username,
            'type_id' => $type,
            'password' => password_hash($password, PASSWORD_BCRYPT),
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name
        ]);
    }

    public static function get($id = null, $username = null, $type = null, $email = null)
    {
        $where_clause = new \WhereClause('and');

        if (!is_null($id)) {
            $where_clause->add('id = %i', $id);
        }
        if (!is_null($username)) {
            $where_clause->add('username = %s', $username);
        }
        if (!is_null($type)) {
            $where_clause->add('type = %s', $type);
        }
        if (!is_null($email)) {
            $where_clause->add('email = %s', $email);
        }

        $result = \DB::query("SELECT id, username, type_id, email, first_name, last_name FROM " . User::table_name . " WHERE %l", $where_clause);

        foreach ($result as &$user) {
            $user["id"] = intval($user["id"]);
            $user["type_id"] = intval($user["type_id"]);
        }

        return $result;
    }

    public static function getAllSorted()
    {
        $result = \DB::query("SELECT id, username, type_id, email, first_name, last_name FROM " . User::table_name . " ORDER BY last_name, first_name, username");

        foreach ($result as &$user) {
            $user["id"] = intval($user["id"]);
            $user["type_id"] = intval($user["type_id"]);
        }

        return $result;
    }

    public static function insert_auth($user_id, $sid, $token)
    {
        return \DB::query("INSERT INTO `" . User::auth_tokens_table_name . "` (user_id, selector, hashed_validator) VALUES (%i, UNHEX(%s), %s)", $user_id, $sid, hash("sha256", $token));
    }

    public static function get_auth($user_id = null, $sid = null, $token = null)
    {
        $where_clause = new \WhereClause('and');

        if (!is_null($user_id)) {
            $where_clause->add('user_id = %i', $user_id);
        }
        if (!is_null($sid)) {
            $where_clause->add('selector = UNHEX(%s)', $sid);
        }
        if (!is_null($token)) {
            $where_clause->add('hashed_validator = %s', hash("sha256", $token));
        }

        $result = \DB::query("SELECT * FROM `" . User::auth_tokens_table_name . "` WHERE %l", $where_clause);

        foreach ($result as &$auth) {
            $auth["user_id"] = intval($auth["user_id"]);
        }

        return $result;
    }

    public static function update_auth_token($user_id = null, $sid = null, $token)
    {
        $where_clause = new \WhereClause('and');

        if (!is_null($user_id)) {
            $where_clause->add('user_id = %i', $user_id);
        }
        if (!is_null($sid)) {
            $where_clause->add('selector = UNHEX(%s)', $sid);
        }

        return \DB::update(User::auth_tokens_table_name, [
            "hashed_validator" => hash("sha256", $token),
            "updated" => \DB::sqlEval("CURRENT_TIMESTAMP()")
        ], "%l", $where_clause);
    }

    public static function delete_auth($user_id = null, $sid = null, $token = null)
    {
        $where_clause = new \WhereClause('and');

        if (!is_null($user_id)) {
            $where_clause->add('user_id = %i', $user_id);
        }
        if (!is_null($sid)) {
            $where_clause->add('selector = UNHEX(%s)', $sid);
        }
        if (!is_null($token)) {
            $where_clause->add('hashed_validator = %s', hash("sha256", $token));
        }

        return \DB::delete(User::auth_tokens_table_name, '%l', $where_clause);
    }
}