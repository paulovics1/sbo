<?php

namespace SBO;

abstract class Utils
{
    static function unsetCookie($cookie)
    {
        unset($_COOKIE[$cookie]);
        setcookie($cookie, null, -1, '/');
    }
}