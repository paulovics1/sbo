<?php

define("ROOT", "../../");

require_once ROOT . 'vendor/autoload.php';

/*------------------ DB (begin) ------------------*/

$dotenv = new Dotenv\Dotenv(ROOT);
$dotenv->overload();

DB::$error_handler = false;
DB::$throw_exception_on_error = true;

DB::$user = getenv("DB_USER");
DB::$password = getenv("DB_PASS");
DB::$dbName = getenv("DB_NAME");
DB::$host = getenv("DB_HOST");

/*------------------- DB (end) -------------------*/