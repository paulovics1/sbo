<?php

use SBO\Controller\Competition;
use SBO\Controller\Game;
use SBO\Controller\User;

require_once "config.php";

switch ($_REQUEST["action"]) {
    case "user_register":
        User::registerAction($_REQUEST["first_name"], $_REQUEST["last_name"], $_REQUEST["email"], $_REQUEST["username"], $_REQUEST["password"]);
        break;
    case "user_login":
        User::logInAction($_REQUEST["username"], $_REQUEST["password"], $_REQUEST["remember"]);
        break;
    case "user_logout":
        User::logOutAction();
        break;
    case "fetch_logged_user_data":
        User::fetchLoggedUserDataAction();
        break;
    case "fetch_all_users_data":
        User::fetchAllUsersDataAction();
        break;
    case "create_competition":
        Competition::createAction($_REQUEST["name"], intval($_REQUEST["type_id"]), array_map("intval", $_REQUEST["skater_ids"]));
        break;
    case "fetch_filtered_competition_ids":
        Competition::fetchFilteredCompetitionIdsAction($_REQUEST["search"], intval($_REQUEST["filter"]));
        break;
    case "fetch_selected_competitions":
        Competition::fetchSelectedCompetitionsAction(array_map("intval", $_REQUEST["competition_ids"]));
        break;
    case "fetch_competition_games":
        Game::fetchCompetitionGamesAction(intval($_REQUEST["competition_id"]));
        break;
}