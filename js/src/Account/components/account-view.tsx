import * as React from "react";
import MainState from "../../App/flux/main-state";
import {Dispatch} from "react-redux";
import AuthenticationView from "./authentication-view";
import {GlobalSnackbarAccess} from "../../App/components/app";
import {RouteComponentProps, withRouter} from "react-router-dom";
import {RaisedButton} from "material-ui";
import Axios from "axios";
import {updateLoggedUserData} from "../../User/utils/user-utils";
import {UserData} from "../../User/user";

export interface AccountViewProps extends GlobalSnackbarAccess, RouteComponentProps<any> {
    loggedUser?: null | UserData,
    dispatch: Dispatch<MainState>
}

export class AccountView extends React.PureComponent<AccountViewProps> {
    handleLogOut = () => {
        let params = new URLSearchParams();
        params.append('action', 'user_logout');

        Axios.post("/SBO/scripts/post.php", params)
            .then(response => {
                if (typeof response.data !== "object") {
                    this.props.showSnackbar({
                        message: response.data
                    });
                    console.error(response.data);
                }
                else {
                    if (response.data.messages.global) {
                        this.props.showSnackbar({
                            message: response.data.messages.global
                        });
                    }

                    if (response.data.success) {
                        updateLoggedUserData(this.props.dispatch);
                    }
                }
            }, response => {
                this.props.showSnackbar({
                    message: response.data
                });
                console.error(response.data);
            });
    };

    render () {
        if (this.props.loggedUser) {
            return (
                <RaisedButton
                    onClick={this.handleLogOut}
                    label={"Log out"}
                    secondary={true}
                />
            );
        }

        return (
            <AuthenticationView
                showSnackbar={this.props.showSnackbar}
                dispatch={this.props.dispatch}
            />
        );
    }
}

export default withRouter(AccountView);