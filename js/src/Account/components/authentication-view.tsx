import * as React from "react";
import {ComponentClass, FormEvent} from "react";
import {grey800, white} from "material-ui/styles/colors";
import {Checkbox, Paper, RaisedButton, Tab, Tabs, TextField} from "material-ui";
import muiThemeable from "material-ui/styles/muiThemeable";
import {MuiTheme} from "material-ui/styles";
import {Link, RouteComponentProps, withRouter} from "react-router-dom";
import Axios from "axios";
import {GlobalSnackbarAccess} from "../../App/components/app";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import {updateLoggedUserData} from "../../User/utils/user-utils";

const styles = {
    textField: {
        width: "100%",
        padding: "5px 0"
    }
};

export interface AuthenticationViewProps extends RouteComponentProps<any>, GlobalSnackbarAccess {
    muiTheme?: MuiTheme,
    dispatch: Dispatch<MainState>
}

export interface AuthenticationViewState {
    messages: {
        login: {[context: string]: string},
        register: {[context: string]: string}
    },
    rememberLogin: boolean
}

export class AuthenticationView extends React.PureComponent<AuthenticationViewProps, AuthenticationViewState> {
    static readonly links = ["/login", "/register"];

    refs: {
        remember: Checkbox
    };

    constructor (props: AuthenticationViewProps) {
        super(props);

        this.state = {
            messages: {
                login: {},
                register: {}
            },
            rememberLogin: false
        };
    }

    getTabSelectedValue () {
        let index = AuthenticationView.links.findIndex(link => this.props.location.pathname.startsWith(`/profile${link}`));
        if (index === -1) {
            index = 0;
        }
        return AuthenticationView.links[index];
    }

    handleSubmitLogIn = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const data = new FormData(event.target as HTMLFormElement);
        data.append("remember", this.state.rememberLogin ? "1" : "0");
        data.append("action", "user_login");

        Axios.post("/SBO/scripts/post.php", data)
            .then(response => {
                if (typeof response.data !== "object") {
                    this.props.showSnackbar({
                        message: response.data
                    });
                    console.error(response.data);
                }
                else {
                    this.setState(prevState => ({
                        messages: {
                            ...prevState.messages,
                            login: response.data.messages
                        }
                    }), () => {
                        if (response.data.messages.global) {
                            this.props.showSnackbar({
                                message: response.data.messages.global
                            });
                        }
                    });

                    if (response.data.success) {
                        this.props.history.replace("/");
                        updateLoggedUserData(this.props.dispatch);
                    }
                }
            }, response => {
                this.props.showSnackbar({
                    message: response.data
                });

            });

        return false;
    };

    handleSubmitRegister = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();

        const data = new FormData(event.target as HTMLFormElement);

        if (data.get("password") !== data.get("password-repeat")) {
            return this.setState(prevState => ({
                messages: {
                    ...prevState.messages,
                    register: {
                        password_repeat: "The passwords don't match."
                    }
                }
            }));
        }
        data.delete("password-repeat");
        data.append("action", "user_register");

        Axios.post("/SBO/scripts/post.php", data)
            .then(response => {
                if (typeof response.data !== "object") {
                    this.props.showSnackbar({
                        message: response.data
                    });
                    console.error(response.data);
                }
                else {
                    this.setState(prevState => ({
                        messages: {
                            ...prevState.messages,
                            register: response.data.messages
                        }
                    }), () => {
                        if (response.data.messages.global) {
                            this.props.showSnackbar({
                                message: response.data.messages.global
                            });
                        }
                    });
                }
            }, response => {
                this.props.showSnackbar({
                    message: response.data
                });
            });

        return false;
    };

    handleRememberLoginCheck = (event: React.MouseEvent<{}>, checked: boolean) => {
        this.setState({
            rememberLogin: checked
        });
    };

    render () {
        const textFieldStyle = {
            style: {...styles.textField},
            underlineFocusStyle: {
                borderColor: this.props.muiTheme!.palette!.accent1Color
            },
            floatingLabelFocusStyle: {
                color: this.props.muiTheme!.palette!.accent1Color
            }
        };
        return (
            <Paper
                zDepth={2}
                style={{
                    background: grey800,
                    margin: "20px",
                    overflow: "auto",
                    maxWidth: "400px"

                }}
            >
                <Tabs
                    value={this.getTabSelectedValue()}
                    tabItemContainerStyle={{
                        background: this.props.muiTheme!.palette!.accent1Color
                    }}
                    contentContainerStyle={{
                        padding: "0 20px 20px"
                    }}
                    inkBarStyle={{
                        background: white
                    }}
                >
                    <Tab
                        label="Log In"
                        value="/login"
                        containerElement={<Link to={"/account/login"}/>}
                    >
                        <form
                            method="POST"
                            onSubmit={this.handleSubmitLogIn}
                        >
                            <TextField
                                {...textFieldStyle}
                                name="username"
                                floatingLabelText="username"
                                errorText={this.state.messages.login["username"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name="password"
                                type="password"
                                autoComplete="false"
                                floatingLabelText="password"
                                errorText={this.state.messages.login["password"] || ""}
                            />
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    paddingTop: "10px",
                                    justifyContent: "space-around",
                                    alignContent: "baseline"
                                }}
                            >
                                <RaisedButton
                                    type="submit"
                                    label="Log in"
                                    secondary={true}
                                />
                                <Checkbox
                                    onCheck={this.handleRememberLoginCheck}
                                    iconStyle={{
                                        fill: this.props.muiTheme!.palette!.accent1Color
                                    }}
                                    style={{
                                        width: "initial",
                                        marginTop: 10
                                    }}
                                    labelStyle={{width: "initial"}}
                                    label="Remember me"
                                />
                            </div>
                        </form>
                    </Tab>
                    <Tab
                        label="Register"
                        value="/register"
                        containerElement={<Link to={"/account/register"}/>}
                    >
                        <form
                            method="POST"
                            onSubmit={this.handleSubmitRegister}
                        >
                            <TextField
                                {...textFieldStyle}
                                name={"first_name"}
                                floatingLabelText="first name"
                                errorText={this.state.messages.register["first_name"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name={"last_name"}
                                floatingLabelText="last name"
                                errorText={this.state.messages.register["last_name"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name={"email"}
                                type="email"
                                floatingLabelText="email"
                                errorText={this.state.messages.register["email"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name={"username"}
                                floatingLabelText="username"
                                errorText={this.state.messages.register["username"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name={"password"}
                                type="password"
                                autoComplete="false"
                                floatingLabelText="password"
                                errorText={this.state.messages.register["password"] || ""}
                            />
                            <TextField
                                {...textFieldStyle}
                                name={"password-repeat"}
                                type="password"
                                autoComplete="false"
                                floatingLabelText="repeat password"
                                errorText={this.state.messages.register["password_repeat"] || ""}
                            />
                            <div
                                style={{
                                    display: "flex",
                                    flexDirection: "row",
                                    paddingTop: "10px",
                                    justifyContent: "space-around",
                                    alignContent: "baseline"
                                }}
                            >
                                <RaisedButton
                                    type="submit"
                                    secondary={true}
                                    label="Register"
                                />
                            </div>
                        </form>
                    </Tab>
                </Tabs>
            </Paper>
        );
    }
}

export default withRouter(muiThemeable()<ComponentClass<AuthenticationViewProps>, AuthenticationViewProps>(AuthenticationView));