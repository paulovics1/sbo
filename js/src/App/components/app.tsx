import * as React from "react";
import {connect, Dispatch} from "react-redux";
import MainState from "../flux/main-state";
import {Redirect, Route, RouteComponentProps, Switch} from "react-router";
import {WrongPath} from "./wrong-path";
import Navigation from "./navigation";
import {MuiThemeProvider} from "material-ui/styles";
import SBOMuiTheme from "../utils/sbo-mui-theme";
import Competitions from "../../Competition/components/competitions";
import {Paper, Snackbar} from "material-ui";
import {updateLoggedUserData} from "../../User/utils/user-utils";
import {AccountView} from "../../Account/components/account-view";
import Loading from "./loading";
import SnackbarProps = __MaterialUI.SnackbarProps;

interface MainComponentProps {
    applicationState: MainState,
    dispatch: Dispatch<MainState>
}

interface MainComponentState {
    snackbarProps: SnackbarProps
}

export class App extends React.PureComponent<MainComponentProps, MainComponentState> {
    state = {
        snackbarProps: {...defaultSnackbarProps},
        loading: true
    };

    snackbarTimer: number | null = null;

    showSnackbar = (snackbarProps: Partial<SnackbarProps>) => {
        this.setState({
            snackbarProps: Object.assign({...defaultSnackbarProps}, snackbarProps, {open: true})
        }, () => {
            if (this.snackbarTimer !== null) {
                clearTimeout(this.snackbarTimer);
            }
            this.snackbarTimer = window.setTimeout(() => {
                this.setState(previousState => ({
                    snackbarProps: Object.assign({...previousState.snackbarProps}, {open: false})
                }));
                this.snackbarTimer = null;
            }, 3000);
        });
    };

    componentDidMount (): void {
        if (!this.props.applicationState.loggedUser) {
            updateLoggedUserData(this.props.dispatch);
        }
    }

    render () {
        return (
            <MuiThemeProvider muiTheme={SBOMuiTheme}>
                <>
                    <Loading
                        loading={this.props.applicationState.loading}
                    >
                        <Navigation/>
                        <Paper
                            zDepth={2}
                            style={{
                                flex: "1 1 0",
                                display: "flex",
                                flexDirection: "column",
                                alignItems: "center"
                            }}
                        >
                            <Switch>
                                <Route exact path="/" render={() => (
                                    <Redirect to="/competitions"/>
                                )}/>
                                <Route path='/competitions' render={(props: RouteComponentProps<any>) => (
                                    <Competitions
                                        {...props}
                                        competitions={this.props.applicationState.competitions}
                                        games={this.props.applicationState.games}
                                        users={this.props.applicationState.users}
                                        loggedUser={this.props.applicationState.loggedUser}
                                        showSnackbar={this.showSnackbar}
                                        dispatch={this.props.dispatch}
                                    />
                                )}/>
                                <Route path='/account' render={(props: RouteComponentProps<any>) => (
                                    <AccountView
                                        {...props}
                                        loggedUser={this.props.applicationState.loggedUser}
                                        showSnackbar={this.showSnackbar}
                                        dispatch={this.props.dispatch}
                                    />
                                )}/>
                                {/*<Route path='/contact' component={Contact}/>*/}
                                <Route component={WrongPath}/>
                            </Switch>
                        </Paper>
                        <Snackbar
                            {...this.state.snackbarProps}
                        />
                    </Loading>
                </>
            </MuiThemeProvider>
        );
    }
}


export interface GlobalSnackbarAccess {
    showSnackbar: typeof App.prototype.showSnackbar
}

const defaultSnackbarProps = {
    open: false,
    message: ""
};

const mapStateToProps = (state: MainState) => ({
    applicationState: state
});

export default connect(
    mapStateToProps
)(App);
