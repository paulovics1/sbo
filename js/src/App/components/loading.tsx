import * as React from "react";
import {CircularProgress} from "material-ui";

export interface LoadingProps {
    loading: boolean
}

export default class Loading extends React.PureComponent<LoadingProps> {
    render () {
        return (
            <>
                <div
                    style={{
                        flex: "1 1 auto",
                        display: "flex",
                        flexDirection: "column",
                        filter: this.props.loading ? "blur(5px)" : "initial"
                    }}
                >
                    {this.props.children}
                </div>
                <div
                    style={{
                        position: "fixed",
                        width: "100%",
                        height: "100%",
                        display: this.props.loading ? "flex" : "none",
                        background: "#10101088",
                        zIndex: 9999,
                        left: "0",
                        top: "0",
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <CircularProgress
                        size={100}
                    />
                </div>
            </>
        );
    }
}