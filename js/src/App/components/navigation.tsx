import * as React from "react";
import {ReactNode} from "react";
import {AppBar, FontIcon, Tab, Tabs} from "material-ui";
import {Link, withRouter} from "react-router-dom";
import {RouteComponentProps} from "react-router";

export interface NavigationProps extends RouteComponentProps<any> {

}

interface TabDescription {
    icon: ReactNode
    link: string
    label: string
    visible?: () => boolean
}

export class Navigation extends React.PureComponent<NavigationProps> {
    static readonly tabs: TabDescription[] = [
        {
            icon: <FontIcon className="material-icons">public</FontIcon>,
            link: "/competitions",
            label: "COMPETITIONS"
        },
        {
            icon: <FontIcon className="material-icons">school</FontIcon>,
            link: "/tricks",
            label: "TRICKS"
        },
        {
            icon: <FontIcon className="material-icons">person</FontIcon>,
            link: "/users",
            label: "USERS"
        },
        {
            icon: <FontIcon className="material-icons">settings</FontIcon>,
            link: "/account",
            label: "ACCOUNT"
        }
    ];

    getTabSelectedValue = () => {
        return (Navigation.tabs.find(tab => this.props.location.pathname.startsWith(tab.link)) || Navigation.tabs[0]).link;
    };

    static isTabVisible (tabDescription: TabDescription) {
        return !tabDescription.visible || tabDescription.visible();
    }

    render () {
        const tabs = Navigation.tabs.map(tab => (
            <Tab
                style={
                    Navigation.isTabVisible(tab) ? {} : {display: "none"}}

                key={tab.label}
                value={tab.link}
                icon={tab.icon}
                containerElement={<Link to={tab.link} replace={true}/>}
                label={tab.label}
            />
        ));
        return (
            <AppBar
                title={
                    <Link style={{
                        textDecoration: "none",
                        color: "initial"
                    }} to={"/"}>
                        Skate battling online
                    </Link>
                }
                style={{
                    alignItems: "center"
                }}
                iconElementLeft={
                    <Link to={"/"}>
                        <img
                            src={"/images/logo.svg"}
                            style={{
                                height: "32px",
                                marginLeft: "8px"
                            }}
                        />
                    </Link>
                }
                iconElementRight={
                    <Tabs
                        value={this.getTabSelectedValue()}
                    >
                        {
                            tabs
                        }
                    </Tabs>}
                iconStyleRight={{
                    flex: "1 1 0",
                    marginRight: "-24px",
                    marginTop: "0"
                }}
            />
        );
    }
}

export default withRouter(Navigation);