import * as React from "react";

interface WrongPathProps {

}

interface WrongPathState {

}

export class WrongPath extends React.PureComponent<WrongPathProps, WrongPathState> {
    render () {
        return (
            <h1
                style={{
                    textAlign: "center",
                    fontWeight: "normal"
                }}
            >
                The url you have visited is invalid.
            </h1>
        );
    }
}