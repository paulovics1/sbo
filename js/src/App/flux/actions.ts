import {UserData} from "../../User/user";
import {CompetitionData} from "../../Competition/competition";
import {GameData} from "../../Competition/Game/game";

export interface AnyAction {
    type: string
}

export namespace ACTIONS {
    export const SET_LOADING = "SET_LOADING";
    export const SET_LOGGED_USER_DATA = "SET_LOGGED_USER_DATA";
    export const SET_ALL_USER_DATA = "SET_ALL_USER_DATA";
    export const UPDATE_COMPETITIONS = "UPDATE_COMPETITIONS";
    export const UPDATE_GAMES = "UPDATE_GAMES";

    export function CREATE_SET_LOADING (value: boolean): PAYLOAD_SET_LOADING {
        return {
            type: SET_LOADING,
            value
        };
    }

    export function CREATE_SET_LOGGED_USER_DATA (userData: null | UserData): PAYLOAD_SET_LOGGED_USER_DATA {
        return {
            type: SET_LOGGED_USER_DATA,
            userData
        };
    }

    export function CREATE_SET_ALL_USER_DATA (userData: UserData[]): PAYLOAD_SET_ALL_USER_DATA {
        return {
            type: SET_ALL_USER_DATA,
            userData
        };
    }

    export function CREATE_UPDATE_COMPETITIONS (competitionData: CompetitionData[]): PAYLOAD_UPDATE_COMPETITIONS {
        return {
            type: UPDATE_COMPETITIONS,
            competitionData: competitionData
        };
    }

    export function CREATE_UPDATE_GAMES (gameData: GameData[]): PAYLOAD_UPDATE_GAMES {
        return {
            type: UPDATE_GAMES,
            gameData: gameData
        };
    }

    export interface PAYLOAD_SET_LOADING extends AnyAction {
        value: boolean
    }

    export interface PAYLOAD_SET_LOGGED_USER_DATA extends AnyAction {
        userData: null | UserData
    }

    export interface PAYLOAD_SET_ALL_USER_DATA extends AnyAction {
        userData: UserData[]
    }

    export interface PAYLOAD_UPDATE_COMPETITIONS extends AnyAction {
        competitionData: CompetitionData[]
    }

    export interface PAYLOAD_UPDATE_GAMES extends AnyAction {
        gameData: GameData[]
    }
}

export default ACTIONS;