import {AnyAction, default as ACTIONS} from "./actions";
import {createNewEmptyMainState, default as MainState} from "./main-state";
import {Reducer} from "redux";
import {appendNormalizedToState, newNormalizedStateIdCategory, NormalizedStateIdCategory, setInState} from "../utils/util";
import {UserData} from "../../User/user";
import {CompetitionData} from "../../Competition/competition";
import {GameData} from "../../Competition/Game/game";

const MainReducer: Reducer<MainState> = (state: MainState = createNewEmptyMainState(), payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.SET_LOADING: {
                const action = payload as ACTIONS.PAYLOAD_SET_LOADING;

                state = setInState(state, ["loading"], action.value, true);
                break payloadSwitch;
            }
            case ACTIONS.SET_LOGGED_USER_DATA: {
                const action = payload as ACTIONS.PAYLOAD_SET_LOGGED_USER_DATA;
                state = setInState(state, ["loggedUser"], action.userData, true);
                break payloadSwitch;
            }
            case ACTIONS.SET_ALL_USER_DATA: {
                const action = payload as ACTIONS.PAYLOAD_SET_ALL_USER_DATA;
                let users: NormalizedStateIdCategory<UserData> = newNormalizedStateIdCategory();

                users = appendNormalizedToState(users, [], action.userData);

                state = setInState(state, ["users"], users, true);
                break payloadSwitch;
            }
            case ACTIONS.UPDATE_COMPETITIONS: {
                const action = payload as ACTIONS.PAYLOAD_UPDATE_COMPETITIONS;
                let competitions: NormalizedStateIdCategory<CompetitionData> = state.competitions || newNormalizedStateIdCategory();

                competitions = appendNormalizedToState(competitions, [], action.competitionData);

                state = setInState(state, ["competitions"], competitions, true);
                break payloadSwitch;
            }
            case ACTIONS.UPDATE_GAMES: {
                const action = payload as ACTIONS.PAYLOAD_UPDATE_GAMES;
                let games: NormalizedStateIdCategory<GameData> = state.games || newNormalizedStateIdCategory();

                games = appendNormalizedToState(games, [], action.gameData);

                state = setInState(state, ["games"], games, true);
                break payloadSwitch;
            }
        }
    return state;
};

export default MainReducer;