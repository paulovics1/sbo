import {UserData} from "../../User/user";
import {NormalizedStateIdCategory} from "../utils/util";
import {GameData} from "../../Competition/Game/game";
import {TrickData, TrickInstanceData} from "../../Trick/trick";
import {CompetitionData} from "../../Competition/competition";

export default interface MainState {
    loading: boolean
    loggedUser?: null | UserData
    users?: NormalizedStateIdCategory<UserData>
    competitions?: NormalizedStateIdCategory<CompetitionData>
    games?: NormalizedStateIdCategory<GameData>
    tricks?: NormalizedStateIdCategory<TrickData>
    trickInstances?: NormalizedStateIdCategory<TrickInstanceData>
}

export function createNewEmptyMainState () {
    let state = {
        loading: false
    } as MainState;
    return state;
}