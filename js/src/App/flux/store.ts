import {createStore} from "redux";
import MainReducer from "./main-reducer";
import {chainReducers} from "../utils/util";

const store = createStore(
    chainReducers(
        MainReducer
    )
);

export default store;