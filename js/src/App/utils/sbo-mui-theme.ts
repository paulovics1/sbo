import {darkBaseTheme, getMuiTheme} from 'material-ui/styles';
import {yellowA700} from "material-ui/styles/colors";

const SBOMuiTheme = getMuiTheme(darkBaseTheme, {
    palette: {
        primary1Color: yellowA700
    }
});

export default SBOMuiTheme;
