import * as React from "react";
import {AnyAction, Reducer} from "redux";

export function orderComparables (comp1: any, comp2: any) {
    let first, second;

    if (comp1 < comp2) {
        first = comp1;
        second = comp2;
    }
    else {
        first = comp2;
        second = comp1;
    }

    return [first, second];
}

export function removeFromArray (object: any, array: any[]) {
    const index = array.indexOf(object);

    if (index !== -1) {
        array.splice(index, 1);
        return true;
    }
    return false;
}

export function pushToArrayIfAbsent (object: any, array: any[]) {
    if (array.indexOf(object) === -1) {
        array.push(object);
        return true;
    }

    return false;
}

export function checkForMembers (array: any[]) {
    for (const member of array) {
        if (typeof member === "undefined") {
            return false;
        }
    }
    return true;
}

export class ClassNameRegister<T> {
    private readonly map = new Map<string, T & HasClassName>();

    getAll () {
        return Array.from(this.map.values());
    }

    register (clazz: T & HasClassName) {
        this.map.set(clazz.className, clazz);
    }

    get (type: string) {
        return this.map.get(type);
    }
}

export type Class<V> = {new (...rest: any[]): V};

export type HasClassName = {
    className: string
};

export function noop (arg: any) {
    return arg;
}

export function findFirstGlobalCharOccurence (needle: string, string: string, fromIndex = 0) {
    let parentheseCount = 0;
    let index = 0;

    for (const char of string) {
        switch (char) {
            case "(":
                parentheseCount++;
                break;
            case ")":
                parentheseCount--;
                break;
            case needle:
                if (parentheseCount === 0 && index >= fromIndex) {
                    return index;
                }
                break;
        }
        index++;
    }

    return -1;
}

if (!Element.prototype.matches) {
    Element.prototype.matches = Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
}

if (!Element.prototype.closest) {
    Element.prototype.closest = function (s: any) {
        let el: Element | null = this;
        if (!document.documentElement.contains(el)) {
            return null;
        }
        do {
            if (el.matches(s)) {
                return el;
            }
            el = el.parentElement;
        } while (el !== null);
        return null;
    };
}

export function getTwoPointLinePaddingCoords (sourceX: number, sourceY: number, sourcePadding: number, targetX: number, targetY: number, targetPadding: number, angle: number) {
    const deltaX = targetX - sourceX;
    const deltaY = targetY - sourceY;
    const lineAngle = Math.atan2(deltaY, deltaX);

    angle = (angle * Math.PI / 180);

    const normX1 = Math.cos(lineAngle - angle);
    const normY1 = Math.sin(lineAngle - angle);
    const normX2 = Math.cos(lineAngle + angle);
    const normY2 = Math.sin(lineAngle + angle);

    return {
        source: {
            x: sourceX + (sourcePadding * normX1),
            y: sourceY + (sourcePadding * normY1)
        },
        target: {
            x: targetX - (targetPadding * normX2),
            y: targetY - (targetPadding * normY2)
        },
        angle: lineAngle
    };
}

export function preventDefault (e: React.SyntheticEvent<any>) {
    e.preventDefault();
}

export function ellipsize (text: string, lettersToShow: number) {
    return text.length < lettersToShow + 1 ? text : text.substr(0, lettersToShow) + "…";
}

export function escapeHtml (text: string) {
    const map: {[str: string]: string} = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function (m) {
        return map[m];
    });
}

export function getSVGEventCoordinates (e: React.MouseEvent<SVGSVGElement>) {
    const point = e.currentTarget.createSVGPoint();

    point.x = e.clientX;
    point.y = e.clientY;

    return point.matrixTransform(e.currentTarget.getScreenCTM()!.inverse());
}

export interface HasId {
    id: number
}

export function chainReducers (...reducers: Reducer<any>[]) {
    return ((state: any, action: AnyAction) => {
        reducers.forEach(reducer => {
            state = reducer(state, action);
        });
        return state;
    }) as Reducer<any>;
}

export function initNormalizedCategoryInState (state: any, newCategory: string) {
    if (typeof state[newCategory] === "undefined") {
        state = {...state};
        state[newCategory] = {
            byId: {},
            allIds: []
        };
    }
    return state;
}

export function setInState (state: any, path: any[], value: any, overwrite: boolean) {
    let result;
    const key = path[0];

    if (path.length > 1) {
        const toAppend: any = setInState(state[key], path.slice(1), value, overwrite);
        result = {...state};
        result[key] = toAppend;
    }
    else {
        result = {...state};
        if (overwrite || typeof result[key] === "undefined") {
            result[key] = value;
        }
    }

    return result;
}

export function appendToStateArray (state: any, path: any[], value: any, appendIfPresent: boolean) {
    let result;
    const key = path[0];

    if (path.length > 1) {
        const toAppend: any = appendToStateArray(state[key], path.slice(1), value, appendIfPresent);
        result = {...state};
        result[key] = toAppend;
    }
    else {
        result = {...state};
        result[key] = [...result[key]];

        if (!Array.isArray(value)) {
            value = [value];
        }
        for (const item of value) {
            if (appendIfPresent || result[key].indexOf(item) === -1) {
                result[key].push(item);
            }
        }
    }

    return result;
}

export function mergeObjectToState (state: any, path: any[], object: Object) {
    let result;

    if (path.length > 0) {
        const key = path[0];
        const toAppend: any = mergeObjectToState(state[key], path.slice(1), object);
        result = {...state};
        result[key] = toAppend;
    }
    else {
        result = {...state};
        Object.assign(result, object);
    }

    return result;
}

export function unsetInState (state: any, path: any[]) {
    let result;

    if (path.length > 1) {
        const key = path[0];
        const toAppend: any = unsetInState(state[key], path.slice(1));
        result = {...state};
        result[key] = toAppend;
    }
    else {
        if (Array.isArray(state)) {
            result = [...state];

            if (Array.isArray(path[0])) {
                for (const trueKey of path[0]) {
                    removeFromArray(trueKey, result);
                }
            }
            else {
                removeFromArray(path[0], result);
            }
        }
        else {
            result = {...state};

            if (Array.isArray(path[0])) {
                for (const trueKey of path[0]) {
                    delete result[trueKey];
                }
            }
            else {
                delete result[path[0]];
            }
        }
    }
    return result;
}

export function appendNormalizedToState (state: any, path: any[], value: HasId | HasId[]) {
    if (!Array.isArray(value)) {
        value = [value];
    }

    const ids = value.map(item => {
        if (typeof item.id === "undefined" || Number.isNaN(item.id)) {
            let category = state;
            for (const key of path) {
                category = category[key];
            }
            item.id = getNewNormalizedId(category);
        }
        return item.id;
    });

    for (const item of value) {
        state = setInState(state, [...path, "byId", item.id], item, true);
    }

    state = appendToStateArray(state, [...path, "allIds"], ids, false);
    return state;
}

export function removeNormalizedFromState (state: any, path: any[], value: number | number[]) {
    state = unsetInState(state, [...path, "byId", value]);
    state = unsetInState(state, [...path, "allIds", value]);
    return state;
}

export type NormalizedStateIdCategory<Object extends NormalizedStateIdObject> = {
    byId: {
        [id: number]: Object
    },
    allIds: number[]
}

export type NormalizedStateIdObject = HasId;

export function* getNormalizedStateIdCategoryIterator<C extends NormalizedStateIdObject> (category: NormalizedStateIdCategory<C>) {
    for (const id of category.allIds) {
        yield category.byId[id];
    }
}

export function mapNormalizedStateIdCategory<C extends NormalizedStateIdObject, R> (category: NormalizedStateIdCategory<C>, mappingFunction: (item: C, index: number) => R): R[] {
    return Array.from(getNormalizedStateIdCategoryIterator(category)).map(mappingFunction);
}

export function filterNormalizedStateIdCategory<C extends NormalizedStateIdObject> (category: NormalizedStateIdCategory<C>, filter: number[] | ((item: C) => boolean)): NormalizedStateIdCategory<C> {
    const resultById: {[id: number]: C} = {};
    const resultAllIds = [];

    if (typeof filter === "function") {
        for (const item of getNormalizedStateIdCategoryIterator(category)) {
            if (filter(item)) {
                resultById[item.id] = item;
                resultAllIds.push(item.id);
            }
        }
    }
    else {
        for (const id of filter) {
            resultById[id] = category.byId[id];
            resultAllIds.push(id);
        }
    }

    return {
        byId: resultById,
        allIds: resultAllIds
    };
}

export function findInNormalizedStateIdCategory<C extends NormalizedStateIdObject> (category: NormalizedStateIdCategory<C>, predicate: (item: C) => boolean, idWhitelist?: number[]): C | null {

    if (idWhitelist) {
        for (const id of idWhitelist) {
            if (predicate(category.byId[id])) {
                return category.byId[id];
            }
        }
    }
    else {
        for (const item of getNormalizedStateIdCategoryIterator(category)) {
            if (predicate(item)) {
                return item;
            }
        }
    }

    return null;
}

export function getNewNormalizedId<C extends NormalizedStateIdObject> (category: NormalizedStateIdCategory<C>) {
    return Math.max(0, ...category.allIds) + 1;
}

export function getRoundedRectanglePath (x: number, y: number, width: number, height: number, topLeft: number, topRight: number, bottomRight: number, bottomLeft: number) {
    let path = `M${x + (width) / 2},${y} `;

    if (topRight) {
        path += `h${width / 2 - topRight} a${topRight},${topRight} 0 0 1 ${topRight},${topRight} v${height / 2 - topRight} `;
    }
    else {
        path += `h${width / 2} v${height / 2} `;
    }
    if (bottomRight) {
        path += `v${height / 2 - bottomRight} a${bottomRight},${bottomRight} 0 0 1 ${-bottomRight},${bottomRight} h${-(width / 2 - bottomRight)} `;
    }
    else {
        path += `v${height / 2} h${-width / 2} `;
    }
    if (bottomLeft) {
        path += `h${-(width / 2 - bottomLeft)} a${bottomLeft},${bottomLeft} 0 0 1 ${-bottomLeft},${-bottomLeft} v${-(height / 2 - bottomLeft)} `;
    }
    else {
        path += `h${-width / 2} v${-height / 2} `;
    }
    if (topLeft) {
        path += `v${-(height / 2 - topLeft)} a${topLeft},${topLeft} 0 0 1 ${topLeft},${-topLeft} z`;
    }
    else {
        path += `v${-height / 2} z`;
    }
    return path;
}


type Sentence = {
    id: number,
    text: string
}

export function getWrappedSVGData (sentences: Sentence[], measureSVGText: (text: string) => Measure2D, separator: string, maxWidth: number, preferNoBreaking: true, sentencePadding = 0) {
    const result = [];
    const separatorWidth = measureSVGText(separator).width;
    const spaceWidth = measureSVGText(`${separator} ${separator}`).width - 2 * separatorWidth;

    let isRowEmpty = true;
    let currentRow = 0;

    const defaultWidthLimit = maxWidth;
    let widthLimit = defaultWidthLimit;

    for (let i = 0; i < sentences.length; i++) {
        const sentence = sentences[i];
        const words = sentence.text.split(" ").filter(val => val);
        widthLimit -= sentencePadding;

        if (words) {
            let fromWord = 0;
            let toWord = 0;

            while (fromWord < words.length) {
                let toWordBot = fromWord;
                let toWordUp = words.length;

                while (toWordBot !== toWordUp) {
                    let text: string;
                    let bBoxWidth: number;
                    toWord = Math.ceil((toWordBot + toWordUp) / 2);
                    let currentWords = words.slice(fromWord, toWord);

                    text = currentWords.join(" ");

                    bBoxWidth = measureSVGText(text).width;

                    if (toWord === words.length) {
                        bBoxWidth += sentencePadding;
                        if (i < sentences.length - 1) {
                            bBoxWidth += separatorWidth;
                        }
                    }

                    if (bBoxWidth > widthLimit) {
                        toWordUp = toWord - 1;
                    }
                    else {
                        toWordBot = toWord;
                    }
                }

                toWord = toWordUp;

                if (preferNoBreaking && toWord < words.length) {
                    let text: string;
                    let bBoxWidth: number;
                    text = words.join(" ");

                    bBoxWidth = measureSVGText(text).width;
                    bBoxWidth += 2 * sentencePadding;
                    if (i < sentences.length - 1) {
                        bBoxWidth += separatorWidth;
                    }

                    if (defaultWidthLimit - bBoxWidth >= 0) {
                        toWord = words.length;
                        currentRow++;
                        isRowEmpty = true;
                        widthLimit = defaultWidthLimit - sentencePadding;
                    }
                }

                if (toWord === fromWord) {
                    if (!isRowEmpty) {
                        currentRow++;
                        isRowEmpty = true;
                        widthLimit = defaultWidthLimit - sentencePadding;
                    }
                    else {
                        let toIndexBot = 1;
                        let toIndexUp = words[fromWord].length;

                        let toIndex;
                        while (toIndexBot !== toIndexUp) {
                            let text: string;
                            let bBoxWidth: number;
                            toIndex = Math.ceil((toIndexUp + toIndexBot) / 2);
                            text = words[fromWord].substring(0, toIndex);

                            bBoxWidth = measureSVGText(text).width;
                            if (toIndex === words[fromWord].length) {
                                bBoxWidth += sentencePadding;
                                if (i < sentences.length - 1 && toWord === words.length - 1) {
                                    bBoxWidth += separatorWidth;
                                }
                            }

                            if (bBoxWidth > widthLimit) {
                                toIndexUp = toIndex - 1;
                            }
                            else {
                                toIndexBot = toIndex;
                            }
                        }

                        toIndex = toIndexUp;

                        let text: string;
                        let bBoxWidth: number;
                        text = words[fromWord].substring(0, toIndex);

                        const textWidth = measureSVGText(text).width;
                        bBoxWidth = textWidth;
                        if (toIndex === words[fromWord].length && toWord === words.length - 1) {
                            bBoxWidth += sentencePadding;
                            if (i < sentences.length - 1) {
                                bBoxWidth += separatorWidth;
                            }
                        }

                        words[fromWord] = words[fromWord].substring(text.length);

                        let leftOffset = defaultWidthLimit - widthLimit;

                        result.push(
                            {
                                id: sentence.id,
                                x: leftOffset,
                                width: textWidth,
                                rowNum: currentRow,
                                text: text
                            }
                        );

                        if (!words[fromWord] && i < sentences.length - 1 && toWord === words.length - 1) {
                            result.push(
                                {
                                    id: "separator",
                                    x: leftOffset + bBoxWidth - separatorWidth,
                                    width: separatorWidth,
                                    rowNum: currentRow,
                                    text: ","
                                }
                            );
                            widthLimit -= spaceWidth;
                        }

                        widthLimit -= bBoxWidth;
                        isRowEmpty = false;

                        if (!words[fromWord]) {
                            fromWord++;
                        }
                    }
                }
                else {
                    let text: string;
                    let bBoxWidth: number;
                    text = words.slice(fromWord, toWord).join(" ");

                    const textWidth = measureSVGText(text).width;
                    bBoxWidth = measureSVGText(text).width;
                    if (toWord === words.length) {
                        bBoxWidth += sentencePadding;
                        if (i < sentences.length - 1) {
                            bBoxWidth += separatorWidth;
                        }
                    }
                    let leftOffset = defaultWidthLimit - widthLimit;

                    result.push(
                        {
                            id: sentence.id,
                            x: leftOffset,
                            width: textWidth,
                            rowNum: currentRow,
                            text: text
                        }
                    );
                    if (i < sentences.length - 1 && toWord === words.length) {
                        result.push(
                            {
                                id: "separator",
                                x: leftOffset + bBoxWidth - separatorWidth,
                                width: separatorWidth,
                                rowNum: currentRow,
                                text: ","
                            }
                        );
                        widthLimit -= spaceWidth;
                    }
                    fromWord = toWord;

                    if (fromWord < words.length) {
                        currentRow++;
                        widthLimit = defaultWidthLimit - sentencePadding;
                        isRowEmpty = true;
                    }
                    else {
                        widthLimit -= bBoxWidth;
                        isRowEmpty = false;
                    }
                }
            }
        }
    }

    return result;
}

export function textToClipboard (text: string) {
    const dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    document.body.removeChild(dummy);
}

export type FunctionWithId<F extends Function> = HasId & F;

export interface Point2D {
    x: number,
    y: number
}

export interface Measure2D {
    height: number
    width: number
}

export interface BoundingBox2D {
    left: number
    top: number
    right: number
    bottom: number
}

export function isPermutationOf<T> (first: T[], second: T[]) {
    if (first.length !== second.length) {
        return false;
    }

    for (const item of first) {
        if (second.indexOf(item) === -1) {
            return false;
        }
    }
    return true;
}

export function isVariationOf<T> (first: T[], second: T[]) {
    if (first.length > second.length) {
        return false;
    }

    for (const item of first) {
        if (second.indexOf(item) === -1) {
            return false;
        }
    }
    return true;
}

export function getBBoxMeasures (element: HTMLElement) {
    const box = element.getBoundingClientRect();

    return {
        width: box.width,
        height: box.height
    };
}

function isBoundingBox2D (object: any): object is BoundingBox2D {
    return 'left' in object && 'right' in object && 'top' in object && 'bottom' in object;
}

export function updateBoundingBox (box: BoundingBox2D, objectBox: BoundingBox2D, padding: number): void;
export function updateBoundingBox (box: BoundingBox2D, point: Point2D, padding: number): void;
export function updateBoundingBox (box: BoundingBox2D, object: Point2D | BoundingBox2D, padding: number): void {
    if (isBoundingBox2D(object)) {
        if (object.left - padding < box.left) {
            box.left = object.left - padding;
        }
        if (object.right + padding > box.right) {
            box.right = object.right + padding;
        }
        if (object.top - padding < box.top) {
            box.top = object.top - padding;
        }
        if (object.bottom + padding > box.bottom) {
            box.bottom = object.bottom + padding;
        }
    }
    else {

        if (object.x - padding < box.left) {
            box.left = object.x - padding;
        }
        if (object.x + padding > box.right) {
            box.right = object.x + padding;
        }
        if (object.y - padding < box.top) {
            box.top = object.y - padding;
        }
        if (object.y + padding > box.bottom) {
            box.bottom = object.y + padding;
        }
    }
}

export function measureSVGText (text: string, svgTextElement: SVGTextElement): Measure2D {
    if (svgTextElement) {
        svgTextElement.textContent = text;
        return svgTextElement.getBBox();
    }

    return {
        height: 0,
        width: 0
    };
}

export function getAllRegexMatches (string: string, regex: RegExp) {
    const result = [];
    let match;

    while ((match = regex.exec(string)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (match.index === regex.lastIndex) {
            regex.lastIndex++;
        }

        result.push(match);
    }

    return result;
}

export function getHTMLElementPageCoords (element: HTMLElement) {
    const box = element.getBoundingClientRect();

    const body = document.body;
    const docEl = document.documentElement;

    const scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    const scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    const clientTop = docEl.clientTop || body.clientTop || 0;
    const clientLeft = docEl.clientLeft || body.clientLeft || 0;

    const top = box.top + scrollTop - clientTop;
    const left = box.left + scrollLeft - clientLeft;

    return {
        top: Math.round(top),
        left: Math.round(left)
    };
}

export function shuffle (array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
}

export function newNormalizedStateIdCategory () {
    return {
        byId: {},
        allIds: []
    };
}