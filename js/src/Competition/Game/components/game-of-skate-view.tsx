import * as React from "react";
import {FlatButton, FloatingActionButton, FontIcon, Paper, RadioButton, RadioButtonGroup, Slider} from "material-ui";
import {Stance, TrickData} from "../../../Trick/trick";
import {ContentClear, NavigationCheck} from "material-ui/svg-icons";
import {grey800, lightGreen600, red600} from "material-ui/styles/colors";

export interface GameOfSkateViewProps {

}

export interface GameOfSkateViewState {
    settingStance: Stance
    settingTrick: TrickData | undefined
    settingBodyRotation: number
}

const styles = {
    radioButton: {
        width: "initial",
        marginTop: "10px",
        marginLeft: "10px"
    }
};

export default class GameOfSkateView extends React.PureComponent<GameOfSkateViewProps, GameOfSkateViewState> {
    constructor (props: GameOfSkateViewProps) {
        super(props);

        this.state = {
            settingBodyRotation: 0,
            settingStance: Stance.REGULAR,
            settingTrick: undefined
        };
    }

    handlebodyRotationChange = (e: React.MouseEvent<{}>, value: number) => {
        this.setState({
            settingBodyRotation: value
        });
    };

    handleStanceChange = (e: React.MouseEvent<{}>, value: any) => {
        this.setState({
            settingStance: value
        });
    };

    render () {
        return (
            <>
                <Paper
                    style={{
                        textAlign: "center",
                        background: grey800,
                        padding: "15px",
                        maxWidth: "600px",
                        width: "100%"
                    }}
                    zDepth={2}
                >
                    <FlatButton
                        label={this.state.settingTrick ? this.state.settingTrick.name : "<choose trick>"}
                    />
                    <RadioButtonGroup
                        name="stance"
                        valueSelected={this.state.settingStance}
                        style={{
                            width: "100%",
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-around",
                            flexWrap: "wrap"
                        }}
                    >
                        <RadioButton
                            style={styles.radioButton}
                            labelStyle={{width: "initial"}}
                            label={"regular"}
                            value={Stance.REGULAR}
                        />
                        <RadioButton
                            style={styles.radioButton}
                            labelStyle={{width: "initial"}}
                            label={"switch"}
                            value={Stance.SWITCH}
                        />
                        <RadioButton
                            style={styles.radioButton}
                            labelStyle={{width: "initial"}}
                            label={"nollie"}
                            value={Stance.NOLLIE}
                        />
                        <RadioButton
                            style={styles.radioButton}
                            labelStyle={{width: "initial"}}
                            label={"fakie"}
                            value={Stance.FAKIE}
                        />
                    </RadioButtonGroup>
                    <Slider
                        sliderStyle={{
                            marginTop: "12px",
                            marginBottom: "12px"
                        }}
                        min={-900}
                        max={900}
                        step={180}
                        value={0}
                        onChange={this.handlebodyRotationChange}
                    />
                    <div>
                        {this.state.settingBodyRotation < 0 ? "BS" : this.state.settingBodyRotation > 0 ? "FS" : ""} {Math.abs(this.state.settingBodyRotation)}
                    </div>

                    <div
                        style={{
                            display: "flex",
                            justifyContent: "space-around"
                        }}
                    >
                        <FloatingActionButton
                            backgroundColor={red600}
                        >
                            <ContentClear/>
                        </FloatingActionButton>

                        <FloatingActionButton
                            backgroundColor={lightGreen600}
                        >
                            <NavigationCheck/>
                        </FloatingActionButton>
                    </div>

                </Paper>

                <table
                    style={{
                        maxWidth: "100%",
                        textAlign: "center"
                    }}
                >
                    <thead>
                    <tr
                        style={{
                            height: "auto"
                        }}

                    >
                        <th
                            style={{
                                height: "175px",
                                whiteSpace: "nowrap",
                                paddingLeft: "0",
                                paddingRight: "0",
                                verticalAlign: "bottom"
                            }}/>
                        {["Jonny Giger", "Jonny Giger", "Jonny Giger", "Jonny Giger", "Jonny Giger", "Jonny Giger", "Jonny Giger", "Jonny Giger"].map(name => (
                            <th
                                style={{
                                    height: "175px",
                                    whiteSpace: "nowrap",
                                    paddingLeft: "0",
                                    paddingRight: "0",
                                    verticalAlign: "bottom"
                                }}
                            >
                                <div
                                    style={{
                                        // backfaceVisibility: "hidden",
                                        boxSizing: "border-box",
                                        transform: " rotate(270deg) translate(50%)",
                                        minWidth: "30px",
                                        width: "0"
                                    }}
                                >
                                    {name}
                                </div>
                            </th>
                        ))}
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td><FontIcon className="material-icons">check</FontIcon></td>
                        <td><FontIcon className="material-icons">clear</FontIcon></td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                        <td>1</td>
                    </tr>
                    <tr>
                        <td>1</td>
                    </tr>
                    </tbody>
                </table>
            </>
        );
    }
}