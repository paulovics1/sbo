export interface GameData {
    id: number
    competitionId: number,
    stage: number,
    gameIndex: number,
    skatersOrder: number[]
}