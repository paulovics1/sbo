import Axios, {AxiosResponse} from "axios";
import {Dispatch} from "react-redux";
import qs from "qs";
import MainState from "../../../App/flux/main-state";
import ACTIONS from "../../../App/flux/actions";

export const updateCompetitionGames = (competitionId: number, dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    Axios.post("/SBO/scripts/post.php", qs.stringify({
        action: 'fetch_competition_games',
        competition_id: competitionId
    }))
        .then(response => {
            if (typeof response.data.games !== "undefined") {
                dispatch(ACTIONS.CREATE_UPDATE_GAMES(response.data.games.map((data: any) => {
                    return {
                        id: data.id,
                        competitionId: data.competition_id,
                        stage: data.stage,
                        gameIndex: data.game_index,
                        skatersOrder: data.skaters_order
                    };
                })));
            }

            !callback || callback(response);
        }, response => {
            !callback || callback(response);
        });
};