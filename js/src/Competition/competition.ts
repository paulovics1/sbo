export enum CompetitionType {
    GAME_OF_SKATE = 0,
    TOURNAMENT = 1
}

export type CompetitionData = {
    id: number
    name: string
    type: CompetitionType
    refereeId: number
    createdTime: string
};

export enum CompetitionFilter {
    ALL,
    ATTENDED_BY_ME,
    ORGANIZED_BY_ME
}