import * as React from "react";
import {ComponentClass} from "react";
import {Avatar, Chip, Dialog, FlatButton, RadioButton, RadioButtonGroup, Subheader, TextField} from "material-ui";
import {RouteComponentProps, withRouter} from "react-router";
import {GlobalSnackbarAccess} from "../../App/components/app";
import {ContentAdd} from "material-ui/svg-icons";
import muiThemeable from "material-ui/styles/muiThemeable";
import {MuiTheme} from "material-ui/styles";
import UserSelectDialog from "../../User/components/user-select-dialog";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import {UserData} from "../../User/user";
import {NormalizedStateIdCategory, removeFromArray} from "../../App/utils/util";
import {green400, grey900, red300} from "material-ui/styles/colors";
import {createCompetition} from "../utils/competition-utils";
import {CompetitionType} from "../competition";
import TournamentCreatingDialog from "./tournament-creating-dialog";

export interface CompetitionCreatingProps extends RouteComponentProps<any>, GlobalSnackbarAccess {
    users: NormalizedStateIdCategory<UserData> | undefined
    dispatch: Dispatch<MainState>
    muiTheme?: MuiTheme
}

export interface CompetitionCreatingState {
    selectedUserIds: number[]
    name: string
    type: CompetitionType
    messages: {
        [context: string]: string
    },
}

const styles = {
    textField: {
        width: "100%"
    },
    flexColumn: {
        padding: "0 15px"
    },
    radioButton: {
        width: "initial",
        marginTop: 10
    }
};

const CompetitionCreatingDefaultState: CompetitionCreatingState = {
    name: "",
    type: CompetitionType.GAME_OF_SKATE,
    selectedUserIds: [],
    messages: {}
};

export class CompetitionCreating extends React.PureComponent<CompetitionCreatingProps, CompetitionCreatingState> {
    constructor (props: CompetitionCreatingProps) {
        super(props);

        this.state = CompetitionCreatingDefaultState;
    }

    refs: {
        typeRadioGroup: RadioButtonGroup,
        nameTextField: TextField,
        userSelectDialog: UserSelectDialog,
        tournamentCreatingDialog: TournamentCreatingDialog
    };

    reset () {
        this.setState(CompetitionCreatingDefaultState);
    }

    handleClose = () => {
        this.props.history.replace(this.props.history.location.pathname.substr(0, this.props.history.location.pathname.length - 4));
    };

    createCompetition () {
        createCompetition(this.state.type, this.state.name, this.state.selectedUserIds, this.props.dispatch, response => {
            if (typeof response.data !== "object") {
                this.props.showSnackbar({
                    message: response.data
                });
                console.error(response.data);
            }
            else {
                if (!response.data.messages) {
                    response.data.messages = {};
                }
                this.setState({
                    messages: response.data.messages
                });

                if (response.data.messages.global) {
                    this.props.showSnackbar({
                        message: response.data.messages.global
                    });
                }
                if (response.data.success) {
                    this.handleClose();
                    this.reset();
                }
            }
        });
    }

    handleCreate = () => {
        if (this.state.type === CompetitionType.TOURNAMENT) {
            this.refs.tournamentCreatingDialog.open();
        }
        else {
            this.createCompetition();
        }
    };

    handleTournamentCreatingDialogConfirm = (userIds: number[]) => {
        this.setState({
            selectedUserIds: userIds
        }, this.createCompetition);
    };

    handleAddSkateboarderClick = () => {
        this.refs.userSelectDialog.open();
    };

    setSelectedUserIds = (selectedUserIds: number[]) => {
        this.setState({
            selectedUserIds: selectedUserIds
        });
    };

    unselectUserId = (userId: number) => {
        this.setState(prevState => {
            let selectedUserIds = [...prevState.selectedUserIds];
            removeFromArray(userId, selectedUserIds);

            return {
                selectedUserIds: selectedUserIds
            };
        });
    };

    isSubmittable () {
        if (this.state.type === CompetitionType.GAME_OF_SKATE) {
            return this.state.selectedUserIds.length >= 2 && this.state.selectedUserIds.length <= 8;
        }
        else {
            return this.state.selectedUserIds.length === 4 ||
                this.state.selectedUserIds.length === 8 ||
                this.state.selectedUserIds.length === 16;
        }
    }

    getSkateboarderCountHint () {
        if (this.state.type === CompetitionType.GAME_OF_SKATE) {
            return "In a Game of S.K.A.T.E., there must be at least 2 skateboarders with the upper limit of 8.";
        }
        else {
            return "In a Tournament, there must be at exactly 4, 8 or 16 skateboarders.";
        }
    }

    handleTypeChange = (e: React.FormEvent<{}>, selected: any) => {
        this.setState({
            type: selected
        });
    };

    handleNameChange = (e: React.FormEvent<{}>, newValue: string) => {
        this.setState({
            name: newValue
        });
    };

    render () {
        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Create"
                primary={true}
                disabled={!this.isSubmittable()}
                onClick={this.handleCreate}
            />
        ];
        return (
            <Dialog
                title="New competition"
                titleStyle={{
                    textAlign: "center"
                }}
                actions={actions}
                modal={false}
                open={this.props.history.location.pathname.endsWith("/new")}
                onRequestClose={this.handleClose}
                contentStyle={{
                    maxWidth: "520px"
                }}
                bodyStyle={{
                    paddingBottom: "0"
                }}

                autoScrollBodyContent={true}
            >
                <RadioButtonGroup
                    ref="type_radiogroup"
                    name="type_radiogroup"
                    valueSelected={this.state.type}
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around"
                    }}
                    onChange={this.handleTypeChange}
                >
                    <RadioButton value={CompetitionType.GAME_OF_SKATE} label={"Game of S.K.A.T.E."}
                                 labelStyle={{width: "initial"}}
                                 style={styles.radioButton}/>
                    <RadioButton value={CompetitionType.TOURNAMENT} label={"Tournament"} labelStyle={{width: "initial"}}
                                 style={styles.radioButton}/>
                </RadioButtonGroup>
                <TextField
                    ref="name_textfield"
                    floatingLabelText="Competition name"
                    onChange={this.handleNameChange}
                    value={this.state.name}
                    errorText={this.state.messages["name"] || ""}
                    style={{
                        ...styles.textField,
                        padding: "0"
                    }}
                />
                <div
                    style={{
                        background: grey900,
                        margin: "20px -2px 0 -2px",
                        padding: "0 15px 15px 15px",
                        display: "flex",
                        flexWrap: "wrap"
                    }}
                >
                    <Subheader
                        style={{
                            padding: "0"
                        }}
                    >
                        List of competitors ({this.state.selectedUserIds.length})
                    </Subheader>
                    <Chip

                        style={{
                            margin: "2px"
                        }}
                        onClick={this.handleAddSkateboarderClick}
                    >
                        <Avatar
                            color={this.props.muiTheme!.palette!.textColor}
                            icon={<ContentAdd/>}/>
                        Add skateboarders
                    </Chip>
                    {
                        !this.props.users ||
                        this.state.selectedUserIds.map(userId => {
                            const user = this.props.users!.byId[userId];

                            return (
                                <Chip
                                    style={{
                                        margin: "2px"
                                    }}
                                    onRequestDelete={() => {this.unselectUserId(userId);}}
                                >
                                    {`${user.firstName} ${user.lastName}`}
                                </Chip>
                            );
                        })
                    }
                </div>
                <span
                    style={{
                        fontSize: "12px",
                        color: this.isSubmittable() ? green400 : red300
                    }}
                >
                    {
                        this.getSkateboarderCountHint()
                    }
                </span>
                <UserSelectDialog
                    ref="userSelectDialog"
                    initialSelectedUserIds={this.state.selectedUserIds}
                    onConfirmSelection={this.setSelectedUserIds}
                    users={this.props.users}
                    dispatch={this.props.dispatch}
                />
                <TournamentCreatingDialog
                    ref="tournamentCreatingDialog"
                    muiTheme={this.props.muiTheme}
                    users={this.props.users}
                    onConfirm={this.handleTournamentCreatingDialogConfirm}
                    userIds={this.state.selectedUserIds}
                />
            </Dialog>
        );
    }
}

export default withRouter(muiThemeable()<ComponentClass<CompetitionCreatingProps>, CompetitionCreatingProps>(CompetitionCreating));