import {Chip, FontIcon, RadioButton, RadioButtonGroup, TextField} from "material-ui";
import * as React from "react";
import {ComponentClass} from "react";
import muiThemeable from "material-ui/styles/muiThemeable";
import {MuiTheme} from "material-ui/styles";
import {CompetitionFilter} from "../competition";

export interface CompetitionListNavProps {
    search: string,
    filter: CompetitionFilter,
    onSearchChange: (newSearch: string) => void
    onFilterChange: (newFilter: CompetitionFilter) => void
    muiTheme?: MuiTheme
}

export interface CompetitionListNavState {
    filterVisible: boolean
}

const styles = {
    radioButton: {
        width: "initial",
        marginTop: "10px",
        marginLeft: "10px"
    }
};

export class CompetitionListNav extends React.PureComponent<CompetitionListNavProps, CompetitionListNavState> {
    static readonly searchUpdateTimeoutMS = 500;

    searchTypeTimeout: number | null;

    constructor (props: CompetitionListNavProps) {
        super(props);

        this.state = {
            filterVisible: false
        };
    }

    toggleFilter = () => {
        this.setState(prevState => ({
            filterVisible: !prevState.filterVisible
        }));
    };

    handleFilterChange = (e: React.FormEvent<{}>, selected: any) => {
        this.props.onFilterChange(selected);
    };

    handleSearchChange = (e: React.FormEvent<{}>, newValue: string) => {
        if (this.searchTypeTimeout !== null) {
            clearTimeout(this.searchTypeTimeout);
            this.searchTypeTimeout = null;
        }

        this.searchTypeTimeout = window.setTimeout(() => {
            this.props.onSearchChange(newValue);
        }, CompetitionListNav.searchUpdateTimeoutMS);
    };

    render () {
        return (
            <>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        padding: "10px 20px 5px 20px"
                    }}
                >
                    <div
                        style={{
                            whiteSpace: "nowrap"
                        }}
                    >
                        <FontIcon
                            className="material-icons"
                            style={{
                                color: this.props.muiTheme!.palette!.secondaryTextColor,
                                right: "4px",
                                verticalAlign: "text-top"
                            }}
                        >
                            search
                        </FontIcon>
                        <TextField
                            onChange={this.handleSearchChange}
                            style={{
                                width: "initial",
                                maxWidth: "100%"
                            }}
                            hintText="Search"
                            defaultValue={this.props.search}
                        />
                    </div>
                    <Chip
                        style={{
                            height: "100%"
                        }}
                        onClick={this.toggleFilter}
                    >
                        Filter
                    </Chip>
                </div>
                <RadioButtonGroup
                    name="filter"
                    style={{
                        width: "100%",
                        display: "flex",
                        flexDirection: "row",
                        justifyContent: "space-around",
                        flexWrap: "wrap"
                    }}
                    valueSelected={this.props.filter}
                    onChange={this.handleFilterChange}
                    className={`toggleable-filters ${this.state.filterVisible ? "toggled-on" : "toggled-off"}`}
                >
                    <RadioButton
                        label="All"
                        style={styles.radioButton}
                        labelStyle={{width: "initial"}}
                        value={CompetitionFilter.ALL}
                    />
                    <RadioButton
                        label="Attended by me"
                        style={styles.radioButton}
                        labelStyle={{width: "initial"}}
                        value={CompetitionFilter.ATTENDED_BY_ME}
                    />
                    <RadioButton
                        label="Organized by me"
                        style={styles.radioButton}
                        labelStyle={{width: "initial"}}
                        value={CompetitionFilter.ORGANIZED_BY_ME}
                    />
                </RadioButtonGroup>
            </>
        );
    }
}

export default muiThemeable()<ComponentClass<CompetitionListNavProps>, CompetitionListNavProps>(CompetitionListNav);