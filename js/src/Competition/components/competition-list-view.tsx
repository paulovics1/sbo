import * as React from "react";
import {ComponentClass} from "react";
import CompetitionList from "./competition-list";
import CompetitionListNav from "./competition-list-nav";
import {UserData} from "../../User/user";
import {Link} from "react-router-dom";
import {FloatingActionButton} from "material-ui";
import {ContentAdd} from "material-ui/svg-icons";
import CompetitionCreating from "./competition-creating";
import muiThemeable from "material-ui/styles/muiThemeable";
import {MuiTheme} from "material-ui/styles";
import {CompetitionData, CompetitionFilter} from "../competition";
import {NormalizedStateIdCategory} from "../../App/utils/util";
import MainState from "../../App/flux/main-state";
import {Dispatch} from "react-redux";
import {GlobalSnackbarAccess} from "../../App/components/app";

export interface CompetitionListViewProps extends GlobalSnackbarAccess {
    loggedUser: UserData | null | undefined
    competitions: NormalizedStateIdCategory<CompetitionData> | undefined
    users: NormalizedStateIdCategory<UserData> | undefined
    dispatch: Dispatch<MainState>
    muiTheme?: MuiTheme
}

export interface CompetitionListViewState {
    search: string,
    filter: CompetitionFilter
}

export class CompetitionListView extends React.PureComponent<CompetitionListViewProps, CompetitionListViewState> {
    constructor (props: CompetitionListViewProps) {
        super(props);

        this.state = {
            search: "",
            filter: CompetitionFilter.ALL
        };
    }

    handleSearchChange = (newSearch: string) => {
        this.setState({
            search: newSearch
        });
    };


    handleFilterChange = (newFilter: CompetitionFilter) => {
        this.setState({
            filter: newFilter
        });
    };

    render () {
        return (
            <>
                <CompetitionListNav
                    search={this.state.search}
                    filter={this.state.filter}
                    onSearchChange={this.handleSearchChange}
                    onFilterChange={this.handleFilterChange}
                />
                <CompetitionList
                    search={this.state.search}
                    filter={this.state.filter}
                    users={this.props.users}
                    competitions={this.props.competitions}
                    dispatch={this.props.dispatch}
                />
                {
                    !this.props.loggedUser ||
                    <>
                        <Link to={"/competitions/new"} replace={true}>
                            <FloatingActionButton
                                style={{
                                    position: "fixed",
                                    right: "30px",
                                    bottom: "30px"
                                }} backgroundColor={this.props.muiTheme!.palette!.primary1Color}>
                                <ContentAdd/>
                            </FloatingActionButton>
                        </Link>

                        <CompetitionCreating
                            users={this.props.users}
                            showSnackbar={this.props.showSnackbar}
                            dispatch={this.props.dispatch}
                        />
                    </>
                }
            </>
        );
    }
}

export default muiThemeable()<ComponentClass<CompetitionListViewProps>, CompetitionListViewProps>(CompetitionListView);