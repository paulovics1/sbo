import {SocialGroup, SocialPoll} from "material-ui/svg-icons";
import {Avatar, CircularProgress, List, ListItem} from "material-ui";
import * as React from "react";
import {NormalizedStateIdCategory} from "../../App/utils/util";
import {CompetitionData, CompetitionFilter, CompetitionType} from "../competition";
import {loadFilteredCompetitions} from "../utils/competition-utils";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import {UserData} from "../../User/user";
import {updateAllUsers} from "../../User/utils/user-utils";
import {Link} from "react-router-dom";

export interface CompetitionListProps {
    competitions: NormalizedStateIdCategory<CompetitionData> | undefined
    users: NormalizedStateIdCategory<UserData> | undefined
    search: string
    filter: CompetitionFilter
    dispatch: Dispatch<MainState>
}

export interface CompetitionListState {
    loading: number,
    filteredCompetitionIds: number[]
}

export default class CompetitionList extends React.PureComponent<CompetitionListProps, CompetitionListState> {
    readonly competitionTypeIcon = {
        [CompetitionType.GAME_OF_SKATE]: <SocialGroup/>,
        [CompetitionType.TOURNAMENT]: <SocialPoll/>
    };

    constructor (props: CompetitionListProps) {
        super(props);

        this.state = {
            loading: 0,
            filteredCompetitionIds: []
        };
    }

    componentDidUpdate (prevProps: Readonly<CompetitionListProps>, prevState: Readonly<CompetitionListState>, snapshot?: any): void {
        if (this.props.search !== prevProps.search || this.props.filter !== prevProps.filter) {
            this.loadFilteredCompetitions();
        }
    }

    loadFilteredCompetitions () {
        this.setState(prevState => ({
            loading: prevState.loading + 1
        }));
        loadFilteredCompetitions(this.props.search, this.props.filter, this.props.dispatch, response => {
            this.setState(prevState => ({
                loading: prevState.loading - 1,
                filteredCompetitionIds: response.data.competition_ids || []
            }));
        });
    }

    updateAllUserData () {
        this.setState(prevState => ({
            loading: prevState.loading + 1
        }));
        updateAllUsers(this.props.dispatch, () => {
            this.setState(prevState => ({
                loading: prevState.loading - 1
            }));
        });
    }

    componentDidMount (): void {
        this.loadFilteredCompetitions();
        if (!this.props.users) {
            this.updateAllUserData();
        }
    }

    render () {
        return (
            <>
                <List
                    style={{
                        width: "100%"
                    }}
                >
                    {
                        this.state.loading || !this.props.competitions ?
                            <CircularProgress
                                style={{
                                    marginLeft: "15px"
                                }}
                            /> :
                            this.state.filteredCompetitionIds.reverse().map(competitionId => this.props.competitions!.byId[competitionId]).sort((a, b) => new Date(b.createdTime).getTime() - new Date(a.createdTime).getTime()).map(competitionData => {
                                const referee = this.props.users!.byId[competitionData.refereeId];
                                return (
                                    <Link to={`/competitions/${competitionData.id}`}>
                                        <ListItem
                                            leftAvatar={<Avatar icon={this.competitionTypeIcon[competitionData.type]}/>}
                                            primaryText={competitionData.name}
                                            secondaryTextLines={2}
                                            secondaryText={(
                                                <div>
                                                <span>
                                                created by {referee.firstName} {referee.lastName}
                                                </span>
                                                    <br/>
                                                    <span
                                                        style={{
                                                            fontSize: "12px"
                                                        }}
                                                    >
                                                {competitionData.createdTime}
                                                </span>
                                                </div>
                                            )}
                                        />
                                    </Link>
                                );
                            })
                    }
                </List>
            </>
        );
    }
}