import {RouteComponentProps, withRouter} from "react-router";
import * as React from "react";
import {CompetitionData, CompetitionType} from "../competition";
import {updateSelectedCompetitions} from "../utils/competition-utils";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import {NormalizedStateIdCategory} from "../../App/utils/util";
import {UserData} from "../../User/user";
import {updateCompetitionGames} from "../Game/utils/game-utils";
import {GameData} from "../Game/game";
import {CircularProgress} from "material-ui";
import GameOfSkateView from "../Game/components/game-of-skate-view";
import {WrongPath} from "../../App/components/wrong-path";
import {updateAllUsers} from "../../User/utils/user-utils";

export interface CompetitionViewProps extends RouteComponentProps<{id: number}> {
    competitionData: CompetitionData | undefined,
    games: NormalizedStateIdCategory<GameData> | undefined,
    users: NormalizedStateIdCategory<UserData> | undefined,
    dispatch: Dispatch<MainState>
}

export interface CompetitionViewState {
    loading: number;
}

export class CompetitionView extends React.PureComponent<CompetitionViewProps, CompetitionViewState> {
    constructor (props: CompetitionViewProps) {
        super(props);

        this.state = {
            loading: 0
        };
    }

    componentDidMount (): void {
        if (!this.props.competitionData) {
            this.setState(prevState => ({
                loading: prevState.loading + 1
            }));
            updateSelectedCompetitions([this.props.match.params.id], this.props.dispatch, () => {
                this.setState(prevState => ({
                    loading: prevState.loading - 1
                }));
            });
        }

        if (!this.props.users) {
            this.setState(prevState => ({
                loading: prevState.loading + 1
            }));
            updateAllUsers(this.props.dispatch, () => {
                this.setState(prevState => ({
                    loading: prevState.loading - 1
                }));
            });
        }

        this.setState(prevState => ({
            loading: prevState.loading + 1
        }));
        updateCompetitionGames(this.props.match.params.id, this.props.dispatch, () => {
            this.setState(prevState => ({
                loading: prevState.loading - 1
            }));
        });
    }

    render () {
        if (this.state.loading || !this.props.competitionData || !this.props.games || !this.props.users) {
            return <CircularProgress
                style={{
                    marginLeft: "15px"
                }}
            />;
        }

        if (this.props.competitionData.type === CompetitionType.GAME_OF_SKATE) {
            return <GameOfSkateView/>;
        }
        else if (this.props.competitionData.type === CompetitionType.TOURNAMENT) {
            return <GameOfSkateView/>;
        }
        return <WrongPath/>;
    }
}

export default withRouter(CompetitionView);