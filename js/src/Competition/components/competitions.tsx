import * as React from "react";
import CompetitionListView from "./competition-list-view";
import {Route, RouteComponentProps, Switch, withRouter} from "react-router";
import {GlobalSnackbarAccess} from "../../App/components/app";
import {UserData} from "../../User/user";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import {NormalizedStateIdCategory} from "../../App/utils/util";
import {CompetitionData} from "../competition";
import CompetitionView from "./competition-view";
import {GameData} from "../Game/game";

export interface CompetitionsProps extends RouteComponentProps<any>, GlobalSnackbarAccess {
    competitions: NormalizedStateIdCategory<CompetitionData> | undefined
    games: NormalizedStateIdCategory<GameData> | undefined
    users: NormalizedStateIdCategory<UserData> | undefined
    loggedUser: UserData | null | undefined
    dispatch: Dispatch<MainState>
}

export interface CompetitionsState {

}

export class Competitions extends React.PureComponent<CompetitionsProps, CompetitionsState> {
    render () {
        return (
            <>
                <Switch>
                    <Route
                        path={this.props.match.url + '/:id'}
                        render={
                            props => (
                                <CompetitionView
                                    key="1"
                                    {...props}
                                    competitionData={!this.props.competitions ? undefined : this.props.competitions.byId[props.match.params.id]}
                                    games={this.props.games}
                                    users={this.props.users}
                                    dispatch={this.props.dispatch}
                                />
                            )
                        }
                    />
                    <Route
                        path={this.props.match.url + '/'}
                        render={
                            (props) => <CompetitionListView
                                {...props}
                                loggedUser={this.props.loggedUser}
                                competitions={this.props.competitions}
                                users={this.props.users}
                                dispatch={this.props.dispatch}
                                showSnackbar={this.props.showSnackbar}
                            />
                        }
                    />
                </Switch>
            </>
        );
    }
}

export default withRouter(Competitions);