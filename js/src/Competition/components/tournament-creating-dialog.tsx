import React, {ReactNode} from "react";
import {Dialog, FlatButton} from "material-ui";
import {grey900} from "material-ui/styles/colors";
import {MuiTheme} from "material-ui/styles";
import {NormalizedStateIdCategory, shuffle} from "../../App/utils/util";
import {UserData} from "../../User/user";

export interface TournamentCreatingDialogProps {
    onConfirm: (userIds: number[]) => void
    users: NormalizedStateIdCategory<UserData> | undefined
    userIds: number[]
    muiTheme?: MuiTheme
}

export interface TournamentCreatingDialogState {
    open: boolean
    userIds: number[]
    selectedCompetitorIndex: number
}

export default class TournamentCreatingDialog extends React.PureComponent<TournamentCreatingDialogProps, TournamentCreatingDialogState> {

    constructor (props: TournamentCreatingDialogProps) {
        super(props);

        this.state = {
            open: false,
            userIds: this.props.userIds,
            selectedCompetitorIndex: -1
        };
    }

    open () {
        this.setState({
            open: true,
            selectedCompetitorIndex: -1
        });
    }

    handleClose = () => {
        this.setState({
            open: false
        });
    };

    handleConfirm = () => {
        this.props.onConfirm(this.state.userIds);
        this.handleClose();
    };

    static getDerivedStateFromProps (nextProps: TournamentCreatingDialogProps, prevState: TournamentCreatingDialogState) {
        return {
            userIds: nextProps.userIds
        };
    }


    handleCompetitorClick = (index: number) => {
        if (this.state.selectedCompetitorIndex === -1) {
            this.setState({
                selectedCompetitorIndex: index
            });
        }
        else {
            this.setState(prevstate => {
                const newUserIds = [...prevstate.userIds];
                newUserIds[index] = prevstate.userIds[prevstate.selectedCompetitorIndex];
                newUserIds[prevstate.selectedCompetitorIndex] = prevstate.userIds[index];

                return {
                    userIds: newUserIds,
                    selectedCompetitorIndex: -1
                };
            });
        }
    };

    randomize = () => {
        this.setState(prevState => {
            const newUserIds = [...prevState.userIds];

            return {
                userIds: shuffle(newUserIds)
            };
        });
    };

    render () {
        const padding = 20;
        const rectWidth = 170;
        const rectHeight = 26;
        const rectSpacing = 5;
        const groupSpacing = 20;
        const levelWidth = 12;
        const levels = Math.log2(this.state.userIds.length);
        let edgeStartY: number[] = [];

        const competitors = this.state.userIds.map(userId => this.props.users!.byId[userId])
            .map((user: UserData, index: number) => {
                const xShift = padding + (index < this.state.userIds.length / 2 ? 0 : rectWidth + levels * 2 * levelWidth);
                const yShift = padding + ((index % (this.state.userIds.length / 2)) * (rectHeight + rectSpacing) + Math.floor(0.5 * (index % (this.state.userIds.length / 2))) * groupSpacing);

                edgeStartY.push(yShift + 0.5 * rectHeight);

                return (
                    <g
                        key={index}
                        transform={`translate(${xShift}, ${yShift})`}
                    >
                        <title>{user.firstName} {user.lastName} ({user.username})</title>
                        <rect
                            height={`${rectHeight}px`}
                            width={`${rectWidth}px`}
                            fill={this.state.selectedCompetitorIndex === index ? "url(#rect-bg-sel)" : "url(#rect-bg)"}
                            cursor="pointer"
                            onClick={() => this.handleCompetitorClick(index)}
                            rx="3px"
                            ry="3px"
                        />
                        <text
                            textAnchor="middle"
                            alignmentBaseline="middle"
                            x={`${rectWidth / 2}px`}
                            y={`${rectHeight / 2}px`}
                            style={{
                                pointerEvents: "none",
                                userSelect: "none",
                                font: "bold 16px Times New Roman, Times, serif"
                            }}
                        >
                            {user.firstName.charAt(0)}. {user.lastName}
                        </text>
                    </g>
                );
            });

        let edges: ReactNode[] = [];
        let newEdgeStartY;

        for (let level = 0; level < levels - 1; level++) {
            newEdgeStartY = [];
            const groupCount = this.state.userIds.length / (Math.pow(2, level));
            for (let groupIndex = 0; groupIndex < groupCount; groupIndex += 2) {
                const xShift = groupIndex < groupCount / 2 ?
                    padding + rectWidth + level * levelWidth :
                    padding + rectWidth + (levels * 2 - level) * levelWidth;
                const yShift = edgeStartY[groupIndex];
                const dx = groupIndex < groupCount / 2 ?
                    levelWidth :
                    -levelWidth;

                newEdgeStartY.push((edgeStartY[groupIndex] + edgeStartY[groupIndex + 1]) / 2);
                edges.push(
                    <g
                        key={`${level}-${groupIndex}`}
                    >
                        <path
                            fill="none"
                            stroke="black"
                            strokeWidth="2px"
                            d={`M${xShift} ${yShift} h${dx} V${edgeStartY[groupIndex + 1]} h${-dx}`}
                        />
                    </g>
                );
            }
            edgeStartY = newEdgeStartY;
        }

        edges.push(
            <g
                key="last"
            >
                <path
                    fill="none"
                    stroke="black"
                    strokeWidth="2px"
                    d={`M${padding + rectWidth + (levels - 1) * levelWidth} ${edgeStartY[0]} h${2 * levelWidth}`}
                />
            </g>
        );

        const totalWidth = 2 * (padding + rectWidth + levels * levelWidth);
        const totalHeight = 2 * edgeStartY[0];

        const actions = [
            <FlatButton
                label="Cancel"
                primary={true}
                onClick={this.handleClose}
            />,
            <FlatButton
                label="Randomize"
                primary={true}
                onClick={this.randomize}
            />,
            <FlatButton
                label="Confirm"
                primary={true}
                onClick={this.handleConfirm}
            />
        ];
        return (
            <Dialog
                title="New tournament"
                titleStyle={{
                    textAlign: "center"
                }}
                actions={actions}
                modal={false}
                open={this.state.open}
                onRequestClose={this.handleClose}
                contentStyle={{
                    maxWidth: "525px"
                }}
                bodyStyle={{
                    paddingBottom: "0",
                    textAlign: "center"
                }}

                autoScrollBodyContent={true}
            >
                <svg
                    width={`${totalWidth}px`}
                    height={`${totalHeight}px`}
                    style={{
                        backgroundColor: grey900
                    }}
                >
                    <defs>
                        <linearGradient id="rect-bg" x1="0%" y1="0%" x2="0%" y2="100%">
                            <stop
                                offset="0%"
                                style={{
                                    stopColor: this.props.muiTheme!.palette!.primary1Color
                                }}
                            />
                            <stop
                                offset="100%"
                                style={{
                                    stopColor: this.props.muiTheme!.palette!.canvasColor
                                }}
                            />
                        </linearGradient>
                        <linearGradient id="rect-bg-sel" x1="0%" y1="0%" x2="0%" y2="100%">
                            <stop
                                offset="0%"
                                style={{
                                    stopColor: this.props.muiTheme!.palette!.accent1Color
                                }}
                            />
                            <stop
                                offset="100%"
                                style={{
                                    stopColor: this.props.muiTheme!.palette!.canvasColor
                                }}
                            />
                        </linearGradient>
                    </defs>
                    {
                        edges
                    }
                    {
                        competitors
                    }
                </svg>
                <div>
                    Click on two skateboarders to swap their initial position.
                </div>
            </Dialog>
        );
    }
}