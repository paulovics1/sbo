import Axios, {AxiosResponse} from "axios";
import ACTIONS from "../../App/flux/actions";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";
import qs from "qs";
import {CompetitionFilter} from "../competition";

export const createCompetition = (typeId: number, name: string, skaterIds: number[], dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    Axios.post("/SBO/scripts/post.php", qs.stringify({
        action: 'create_competition',
        type_id: typeId,
        name: name,
        skater_ids: skaterIds
    }))
        .then(response => {
            !callback || callback(response);
        }, response => {
            !callback || callback(response);
        });
};

export const loadFilteredCompetitions = (search: string, filter: CompetitionFilter, dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    Axios.post("/SBO/scripts/post.php", qs.stringify({
        action: 'fetch_filtered_competition_ids',
        search: search,
        filter: filter
    }))
        .then(response => {
            if (typeof response.data.competition_ids !== "undefined") {
                updateSelectedCompetitions(response.data.competition_ids, dispatch,
                    !callback ?
                        undefined :
                        () => {
                            callback(response);
                        }
                );
            }
        }, response => {
            !callback || callback(response);
        });
};

export const updateSelectedCompetitions = (competitionIds: number[], dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    Axios.post("/SBO/scripts/post.php", qs.stringify({
        action: 'fetch_selected_competitions',
        competition_ids: competitionIds
    }))
        .then(response => {
            if (typeof response.data.competitions !== "undefined") {
                dispatch(ACTIONS.CREATE_UPDATE_COMPETITIONS(response.data.competitions.map((data: any) => {
                    return {
                        id: data.id,
                        type: data.type_id,
                        name: data.name,
                        refereeId: data.referee_id,
                        createdTime: data.created_time
                    };
                })));
            }

            !callback || callback(response);
        }, response => {
            !callback || callback(response);
        });
};