export interface TrickData {
    id: number
}

export interface TrickInstanceData {
    id: number
}

export enum Stance {
    REGULAR,
    SWITCH,
    NOLLIE,
    FAKIE
}