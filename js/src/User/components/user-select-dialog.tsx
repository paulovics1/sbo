import * as React from "react";
import {ReactNode} from "react";
import {Avatar, CircularProgress, Dialog, FlatButton, List, ListItem} from "material-ui";
import {mapNormalizedStateIdCategory, NormalizedStateIdCategory, removeFromArray} from "../../App/utils/util";
import {UserData} from "../user";
import {updateAllUsers} from "../utils/user-utils";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";

export interface UserSelectDialogProps {
    users: NormalizedStateIdCategory<UserData> | undefined,
    initialSelectedUserIds: number[]
    onConfirmSelection: (selectedUserIds: number[]) => void
    dispatch: Dispatch<MainState>
}

export interface UserSelectDialogState {
    selectedUserIds: number[],
    open: boolean,
    loading: boolean
}

export default class UserSelectDialog extends React.PureComponent<UserSelectDialogProps, UserSelectDialogState> {
    constructor (props: UserSelectDialogProps) {
        super(props);

        this.state = {
            open: false,
            selectedUserIds: [...this.props.initialSelectedUserIds],
            loading: false
        };
    }

    static getDerivedStateFromProps (nextProps: UserSelectDialogProps, prevState: UserSelectDialogState) {
        return {
            selectedUserIds: [...nextProps.initialSelectedUserIds],
            loading: prevState.loading || !nextProps.users
        };
    }

    open () {
        this.setState({
            open: true
        });

        if (!this.props.users) {
            updateAllUsers(this.props.dispatch, () => {
                this.setState({
                    loading: false
                });
            });
        }
    }

    handleClose = () => {
        this.setState({
            open: false
        });
    };

    handleRefresh = () => {
        this.setState({
            loading: true
        });
        updateAllUsers(this.props.dispatch, () => {
            this.setState({
                loading: false
            });
        });
    };

    handleUserClick = (userId: number) => {
        this.setState(prevState => {

            let newSelection = [...prevState.selectedUserIds];
            if (prevState.selectedUserIds.indexOf(userId) !== -1) {
                removeFromArray(userId, newSelection);
            }
            else {
                newSelection.push(userId);
            }

            return {
                selectedUserIds: newSelection
            };
        });
    };

    handleConfirm = () => {
        this.props.onConfirmSelection(this.state.selectedUserIds);
        this.handleClose();
    };

    render () {
        let content: ReactNode;
        if (this.props.users && !this.state.loading) {
            content = (
                <>
                    <List>
                        {
                            mapNormalizedStateIdCategory(this.props.users, user => (
                                <ListItem
                                    key={user.id}
                                    className={this.state.selectedUserIds.indexOf(user.id) !== -1 ? "list-item--selected" : ""}
                                    onClick={() => {this.handleUserClick(user.id);}}
                                    primaryText={`${user.firstName} ${user.lastName}`}
                                    secondaryText={user.username}
                                    leftAvatar={
                                        <Avatar>{`${user.firstName.charAt(0)}${user.lastName.charAt(0)}`}</Avatar>}
                                />
                            ))
                        }
                    </List>
                    <div
                        style={{
                            textAlign: "center"
                        }}
                    >
                    </div>
                </>
            );
        }
        else {
            content = (
                <CircularProgress/>
            );
        }

        return (
            <Dialog
                title="List of users"
                actions={
                    !this.props.users || this.state.loading ? undefined :
                        [
                            <FlatButton
                                label="Cancel"
                                primary={true}
                                onClick={this.handleClose}
                            />,
                            <FlatButton
                                label="Refresh"
                                primary={true}
                                onClick={this.handleRefresh}
                            />,
                            <FlatButton
                                label="Confirm"
                                primary={true}
                                onClick={this.handleConfirm}
                            />
                        ]
                }
                open={this.state.open}
                modal={false}
                contentStyle={{
                    maxWidth: "400px"
                }}
                onRequestClose={this.handleClose}
                autoScrollBodyContent={true}
            >
                {
                    content
                }
            </Dialog>
        );
    }
}