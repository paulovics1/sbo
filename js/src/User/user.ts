export interface UserData {
    id: number,
    email: string,
    username: string,
    firstName: string,
    lastName: string,
    type: UserType
}

export enum UserType {
    SUPER_ADMIN = 0,
    ADMIN = 1,
    BASIC = 2
}