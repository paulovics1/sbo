import Axios, {AxiosResponse} from "axios";
import ACTIONS from "../../App/flux/actions";
import {Dispatch} from "react-redux";
import MainState from "../../App/flux/main-state";

export const updateLoggedUserData = (dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    dispatch(ACTIONS.CREATE_SET_LOADING(true));
    let params = new URLSearchParams();
    params.append('action', 'fetch_logged_user_data');

    Axios.post("/SBO/scripts/post.php", params)
        .then(response => {
            if (typeof response.data.loggedUser !== "undefined") {
                dispatch(ACTIONS.CREATE_SET_LOGGED_USER_DATA(response.data.loggedUser === null ? null :
                    {
                        id: response.data.loggedUser.id,
                        email: response.data.email,
                        firstName: response.data.loggedUser.first_name,
                        lastName: response.data.loggedUser.last_name,
                        username: response.data.loggedUser.username,
                        type: response.data.loggedUser.type_id
                    }));
            }

            dispatch(ACTIONS.CREATE_SET_LOADING(false));
            !callback || callback(response);
        }, response => {
            !callback || callback(response);
            dispatch(ACTIONS.CREATE_SET_LOADING(false));
        });
};

export const updateAllUsers = (dispatch: Dispatch<MainState>, callback?: (response: AxiosResponse) => void) => {
    let params = new URLSearchParams();
    params.append('action', 'fetch_all_users_data');

    Axios.post("/SBO/scripts/post.php", params)
        .then(response => {
            if (typeof response.data.users !== "undefined") {
                dispatch(ACTIONS.CREATE_SET_ALL_USER_DATA(response.data.users.map((data: any) => {
                    return {
                        id: parseInt(data.id),
                        type: parseInt(data.type_id),
                        username: data.username,
                        email: data.email,
                        firstName: data.first_name,
                        lastName: data.last_name
                    };
                })));
            }

            !callback || callback(response);
        }, response => {
            !callback || callback(response);
        });
};