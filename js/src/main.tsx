import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {BrowserRouter, Route} from 'react-router-dom';
import Main from "./App/components/app";
import store from "./App/flux/store";
import "../../css/src/main.scss";

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Route component={Main}/>
        </BrowserRouter>
    </Provider>,
    document.querySelector('#react-mount')
);