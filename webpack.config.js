const ExtractTextPlugin = require("extract-text-webpack-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const HardSourceWebpackPlugin = require('hard-source-webpack-plugin');
const webpack = require("webpack");


module.exports = {
    mode: "development",
    entry: {
        main: ["babel-polyfill", "./js/src/main.tsx"]
    },
    output: {
        path: __dirname + "/js/build",
        filename: "[name].js",
        chunkFilename: "[name].js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js"]
    },
    devtool: "source-map",
    module: {
        exprContextCritical: false,
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['awesome-typescript-loader']
            },
            {
                test: /\.less/,
                loaders: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'less-loader']
                })
            },
            {
                test: /\.scss/,
                loaders: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            }
        ]
    },
    node: {
        fs: 'empty',
        child_process: 'empty'
    },
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        "redux": "Redux",
        "react-redux": "ReactRedux",
        "react-router-dom": "ReactRouterDOM"
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: "vendor",
                    chunks: "all",
                }
            }
        }
    },
    plugins: [
        new HardSourceWebpackPlugin(),
        new ExtractTextPlugin({
            filename: "../../css/build/[name].css",
            allChunks: true
        }),
        // new BundleAnalyzerPlugin({
        //     analyzerMode: 'static',
        //     reportFilename: '../../report.html',
        //     defaultSizes: 'parsed',
        //     openAnalyzer: false,
        //     generateStatsFile: true,
        //     statsFilename: '../../stats.json',
        //     statsOptions: null,
        //     logLevel: 'info'
        // })
    ]
};